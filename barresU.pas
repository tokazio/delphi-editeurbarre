unit barresU;

interface

uses classes, sysutils, dialogs, forms, windows, functions;

type

  Tactions = (aNone, aAdd, aEdit);
  Tmorceau = class;

  Tmorceau = class
  Private
    Ffilename: string;
    Fgamme: string;
    Fnom: string;
    // description:Tstrings;
    Flongueur: single;
    // anglea:byte;
    // angleb:byte;
    Fpercages: Tstringlist;
    Fcodeprofil: byte;
    procedure setLongueur(const Value: single);
    procedure setNom(const Value: string);
    procedure setCodeProfil(const Value: byte);
    procedure setGamme(const Value: string);
  Published
    property filename: string read Ffilename;
    property gamme: string read Fgamme write setGamme;
    property nom: string read Fnom write setNom;
    property longueur: single read Flongueur write setLongueur;
    property codeProfil: byte read Fcodeprofil write setCodeProfil;
    property percages: Tstringlist read Fpercages;
  Public
    Constructor create; overload;
    Constructor create(filename: string); overload;
    Destructor free;
    function saveToFile(filename: string; overwrite: boolean): boolean;
    procedure assign(morceau: Tmorceau);
  end;

  Tbarre = class
  Public
    nom: string;
    longueur: integer;
    codeProfil: byte;
    description: Tstrings;
    morceaux: Tstringlist;
    procedure duplicate(morceau: Tmorceau);
    procedure exportStrings(strings: Tstrings; num: integer);
    function toFileString: string;
    procedure openMorceaux(s: string);
    Constructor create(nom: string; codeprofile: byte; description: string; longueur: integer);
    Destructor free;
  end;

CONST
  SCIE: integer = 5;

function nomProfil(codeProfil: integer): string;
function fillSpaces(text: string; size: integer): string;

implementation

uses mainFormU;

function nomProfil(codeProfil: integer): string;
var
  l: Tstringlist;
begin
  Result := '?';
  l := Tstringlist.create;
  l.LoadFromFile(extractfilepath(application.ExeName) + 'nomProfiles.txt');
  Result := l.Values[inttostr(codeProfil)];
  l.free;
end;

function fillSpaces(text: string; size: integer): string;
var
  I, m: integer;
begin
  Result := text;
  if length(text) = size then
    exit;
  if length(text) > size then
    Result := copy(text, 1, size)
  else
  begin
    m := size - length(text) - 1;
    for I := 0 to m do
      Result := Result + ' ';
  end;
end;

procedure Tbarre.openMorceaux(s: string);
var
  tmp: Tstringlist;
  I, code, J: integer;
  m: Tmorceau;
begin
  tmp := Tstringlist.create;
  tmp.CommaText := s;
  for I := 0 to tmp.count - 1 do
  begin
    if not fileexists(extractfilepath(application.ExeName) + 'morceaux/' + tmp[I] + '.txt') then
    begin
      // si le morceau n'existe pas
      Val(tmp[I], J, code);
      if code <> 0 then
      begin
        showmessage('Impossible d''ouvrir le morceau ' + tmp[I]);
      end
      else
      begin
        // si c'est une longueur simple
        m := Tmorceau.create;
        m.nom := tmp[I];
        m.longueur := strtoint(tmp[I]);
        morceaux.AddObject(m.nom, m);
      end;
    end
    else
    begin
      // si le morceau existe
      m := Tmorceau.create(extractfilepath(application.ExeName) + 'morceaux/' + tmp[I] + '.txt');
      morceaux.AddObject(m.nom, m)
    end;
  end;
  tmp.free;
end;

procedure Tbarre.duplicate(morceau: Tmorceau);
begin
  morceaux.AddObject(morceau.nom, morceau);
end;

function Tbarre.toFileString;
var
  I, J: integer;
  tmp: Tstringlist;
begin
  tmp := Tstringlist.create;
  tmp.Delimiter := '@';
  tmp.Add('nom=' + nom);
  tmp.Add('longueur=' + inttostr(longueur));
  tmp.Add('codeprofil=' + inttostr(codeProfil));
  tmp.Add('description=' + description.DelimitedText);
  tmp.Add('morceaux=' + morceaux.DelimitedText);
  Result := tmp.DelimitedText;
  tmp.free;
end;

procedure Tbarre.exportStrings(strings: Tstrings; num: integer);
var
  I, J: integer;
  m: Tmorceau;
  poslame: single;
  // dsep,
  tsep: char;
  temps: Tstringlist;
  a: string;
begin
  temps := Tstringlist.create;
  temps.LoadFromFile(extractfilepath(application.ExeName) + 'temps.txt');
  tsep := thousandseparator;
  // dsep := decimalseparator;
  decimalseparator := '.';
  thousandseparator := #0;
  strings.Delimiter := #13;
  // d�finition de la barre  1,2,"BIBOIS9055        ",6975
  strings.Add(inttostr(num) + ',' + inttostr(codeProfil) + ',"' + fillSpaces(nomProfil(codeProfil), 18) + '",' + inttostr(longueur));
  // calcul du temp d'avance
  form8.activeform.temp := round(strtofloat(temps.Values['avance']) * longueur);
  // temp de changement de barre
  form8.activeform.temp := round(strtofloat(temps.Values['barre']));
  // d�finition des coupes et per�ages (et �tiquette si besoin)
  // num barre, 0=percage 1=coupe, pos mm, pos 10eme mm,code percage / angle coupe
  // 1,0,1118,2,6
  // 924,1,19,5,0
  poslame := 0;
  for I := 0 to morceaux.count - 1 do
  begin
    // r�cup le morceau
    m := Tmorceau(morceaux.Objects[I]);
    // ajoute les per�ages
    m.percages.CustomSort(sortPercagesPosInc);
    for J := 0 to m.percages.count - 1 do
    begin
      a := formatfloat('0.0', poslame + (SCIE / 2) + strtofloat(m.percages.ValueFromIndex[J]));
      a := stringReplace(a, '.', ',', [rfReplaceAll, rfIgnoreCase]);
      strings.Add(inttostr(num) + ',0,' + a + ',' + m.percages.Names[J]);
      if temps.IndexOfName('p' + m.percages.Names[J]) >= 0 then
      begin
        form8.activeform.temp := round(form8.activeform.temp + strtofloat(temps.Values['p' + m.percages.Names[J]]));
      end
      else
      begin
        showmessage('Le temps machine pour le per�age ' + m.percages.Names[J] + ' n''est pas d�fini!');
      end;
    end;
    if I > 0 then
    begin
      poslame := poslame + m.longueur + SCIE;
    end
    else
    begin
      poslame := poslame + m.longueur + (SCIE / 2);
    end;
    // strings.Add(inttostr(num)+',1,'+formatfloat('0.0',poslame)+','+inttostr(m.angleb));
    a:=formatfloat('0.0', poslame);
    a := stringReplace(a, '.', ',', [rfReplaceAll, rfIgnoreCase]);
    strings.Add(inttostr(num) + ',1,' + a + ',0');
    form8.activeform.temp := round(form8.activeform.temp + strtofloat(temps.Values['coupe']));
  end;
  // decimalseparator := dsep;
  thousandseparator := tsep;
  temps.free;
end;

constructor Tbarre.create(nom: string; codeprofile: byte; description: string; longueur: integer);
begin
  morceaux := Tstringlist.create;
  self.description := Tstringlist.create;
  self.description.Delimiter := '|';
  self.nom := nom;
  self.codeProfil := codeprofile;
  self.description.DelimitedText := description;
  self.longueur := longueur;
end;

destructor Tbarre.free;
var
  I: integer;
begin
  for I := 0 to morceaux.count - 1 do
  begin
    Tmorceau(morceaux.Objects[I]).free;
    morceaux.Objects[I] := nil;
  end;
  morceaux.Clear;
  morceaux.free;
  description.Clear;
  description.free;
end;

{ Tmorceau }

constructor Tmorceau.create overload;
begin
  inherited create;
  // description:=Tstringlist.Create;
  // description.Delimiter:='|';
  Fpercages := Tstringlist.create;
end;

procedure Tmorceau.assign(morceau: Tmorceau);
begin
  Ffilename := morceau.Ffilename;
  Fnom := morceau.Fnom;
  // description.AddStrings(morceau.description);
  Flongueur := morceau.Flongueur;
  // anglea:=morceau.anglea;
  // angleb:=morceau.angleb;
  Fpercages.assign(morceau.Fpercages);
  Fcodeprofil := morceau.Fcodeprofil;
end;

constructor Tmorceau.create(filename: string)overload;
var
  t: Tstringlist;
begin
  create;
  t := Tstringlist.create;
  t.NameValueSeparator := '>';
  t.LoadFromFile(filename);
  Ffilename := extractfilename(filename);

  Fgamme := t.Values['gamme'];
  if trim(Fgamme) = '' then
    showmessage('Pas de gamme dans ' + Ffilename + '!');
  Fnom := t.Values['nom'];
  if trim(Fnom) = '' then
    showmessage('Pas de nom dans ' + Ffilename + '!');
  trystrtofloat(t.Values['longueur'], Flongueur);
  if Flongueur = 0 then
    showmessage('Pas de longueur ou longueur 0 dans ' + filename + '!');
  // if t.IndexOfName('anglea')>=0 then anglea:=strtoint(t.Values['anglea']) else r:=TRUE;
  // if t.IndexOfName('angleb')>=0 then angleb:=strtoint(t.Values['angleb']) else r:=TRUE;
  // if t.IndexOfName('description')>=0 then description.DelimitedText:=t.Values['description'] else r:=TRUE;
  Fpercages.CommaText := t.Values['percages']; //
  Fcodeprofil := strtoint(t.Values['codeprofil']);
  t.free;
end;

destructor Tmorceau.free;
var
  I: integer;
begin
  for I := 0 to percages.count - 1 do
  begin
    percages.Objects[I].free;
    percages.Objects[I] := nil;
  end;
  percages.free;
  // description.Free;
  // inherited destroy;
end;

function Tmorceau.saveToFile(filename: string; overwrite: boolean): boolean;
var
  f: textfile;
begin
  Result := TRUE;
  if fileexists(filename) and not overwrite then
  begin
    case MessageBox(0, PwideCHar('Le morceau "' + nom + '" existe d�j�. Voulez vous le remplacer?'), 'Ecrasement', +mb_YesNo + mb_ICONWARNING) of
      // yes
      6:
        begin
          Result := TRUE;
        end;
      // no
      7:
        begin
          Result := FALSE;
          exit;
        end;
    end;
  end;
  // description.Delimiter:='|';
  try
    assignfile(f, filename);
    try
      rewrite(f);
      writeln(f, 'filename>' + Ffilename);
      writeln(f, 'gamme>' + Fgamme);
      writeln(f, 'nom>' + Fnom);
      // writeln(f,'description>'+description.DelimitedText);
      writeln(f, 'longueur>' + floattostr(Flongueur));
      // writeln(f,'anglea>'+inttostr(anglea));
      // writeln(f,'angleb>'+inttostr(angleb));
      writeln(f, 'percages>' + percages.CommaText);
      writeln(f, 'codeprofil>' + inttostr(Fcodeprofil));
    finally
      closefile(f);
    end;
  except
    Result := FALSE;
  end;
end;

procedure Tmorceau.setCodeProfil(const Value: byte);
begin
  Fcodeprofil := Value;
end;

procedure Tmorceau.setGamme(const Value: string);
begin
  Fgamme := Value;
end;

procedure Tmorceau.setLongueur(const Value: single);
begin
  Flongueur := Value;
end;

procedure Tmorceau.setNom(const Value: string);
begin
  Fnom := Value;
end;

end.
