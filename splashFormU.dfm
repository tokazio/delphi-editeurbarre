object Form12: TForm12
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Form12'
  ClientHeight = 241
  ClientWidth = 452
  Color = 3365605
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 50
    Width = 446
    Height = 45
    Margins.Top = 50
    Align = alTop
    Alignment = taCenter
    Caption = 'Editeur de barre'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -32
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
    ExplicitLeft = 60
    ExplicitTop = 76
    ExplicitWidth = 219
  end
  object Label2: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 225
    Width = 446
    Height = 13
    Align = alBottom
    Alignment = taRightJustify
    Caption = 'Label2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ExplicitLeft = 164
    ExplicitTop = 172
    ExplicitWidth = 31
  end
end
