object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Export'
  ClientHeight = 337
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 632
    Height = 296
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = []
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 296
    Width = 632
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 16
      Width = 138
      Height = 13
      Caption = 'Temps de fabrication estim'#233':'
    end
    object Label2: TLabel
      Left = 160
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Label2'
    end
    object Button1: TButton
      Left = 540
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Enregistrer'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object FileSaveDialog1: TFileSaveDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'CSV'
        FileMask = '*.CSV'
      end>
    Options = []
    Title = 'Exporter sous...'
    Left = 330
    Top = 204
  end
end
