unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtCtrls;

type
  TForm6 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure loadcodes(filename: string);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button2Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    filename: string;
    edited: Boolean;
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}

procedure TForm6.Button1Click(Sender: TObject);
begin
  if filename <> '' then
  begin
    Memo1.Lines.SaveToFile(filename);
    Label3.Caption := 'Enregistré le ' + datetimeToStr(now) + '. Relancer le programme pour prendre en compte la modification';
    edited := false;
    Caption := 'Editeur de liste';
  end;
end;

procedure TForm6.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm6.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  (*
  Memo1.Clear;
  Label1.Caption := '';
  filename := '';
  *)
  action:=cafree;
end;

procedure TForm6.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if edited then
  begin
    case MessageBox(0, PwideCHar('Enregistrer avant de quitter?'), 'Modifications', +mb_YesNoCancel + mb_ICONWARNING) of
      // yes
      6:
        begin
          Button1Click(Sender);
          CanClose := TRUE;
        end;
      // cancel
      2:
        begin
          CanClose := false;
        end;
      // no
      7:
        begin
          CanClose := TRUE;
        end;
    end;
  end
  else
    CanClose := TRUE;
end;

procedure TForm6.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TForm6.loadcodes(filename: string);
begin
  Label3.Caption := '';
  self.filename := filename;
  Label1.Caption := extractfilename(filename);
  Memo1.Lines.LoadFromFile(filename);
  show;
  Caption := 'Editeur de liste';
  edited := false;
end;

procedure TForm6.Memo1Change(Sender: TObject);
begin
  edited := TRUE;
  Caption := 'Editeur de liste*';
end;

end.
