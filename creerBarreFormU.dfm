object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Cr'#233'er une barre'
  ClientHeight = 219
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    301
    219)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 36
    Width = 39
    Height = 13
    Caption = 'Profil'#233':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 10
    Top = 8
    Width = 28
    Height = 13
    Caption = 'Nom:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 10
    Top = 96
    Width = 57
    Height = 13
    Caption = 'Description:'
  end
  object Label4: TLabel
    Left = 10
    Top = 64
    Width = 56
    Height = 13
    Caption = 'Longueur:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 270
    Top = 64
    Width = 16
    Height = 13
    Caption = 'mm'
  end
  object ComboBox1: TComboBox
    Left = 80
    Top = 32
    Width = 211
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    Text = 'Choisir...'
    OnChange = ComboBox1Change
    Items.Strings = (
      'Choisir...'
      '01- Monoprofil ('#224' plat vers le bas)'
      '02- Biprofil'
      '03- Cache rainure'
      '04- Tube MC diam'#232'tre 50'
      '05- Poteau diam'#232'tre 40 (rainure en bas)'
      '06- Tube Traverse diam'#232'tre 16')
  end
  object Button1: TButton
    Left = 217
    Top = 187
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Enregistrer'
    Enabled = False
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 127
    Top = 187
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 80
    Top = 4
    Width = 211
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = Edit1Change
  end
  object Memo1: TMemo
    Left = 10
    Top = 112
    Width = 281
    Height = 61
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
  end
  object Edit2: TEdit
    Left = 80
    Top = 60
    Width = 181
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    NumbersOnly = True
    TabOrder = 2
    OnChange = Edit2Change
  end
end
