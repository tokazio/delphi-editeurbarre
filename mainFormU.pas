unit mainFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, PlatformDefaultStyleActnCtrls, ActnMan, Menus, functions, barresU, projetFormU,
  ImgList, ComCtrls, ExtCtrls, shellapi, Unit6, Unit9, creerBarreFormU, creerMorceauFormU, exportFormU;

type
  TForm8 = class(TForm)
    MainMenu1: TMainMenu;
    Fichier1: TMenuItem;
    Enregistrer1: TMenuItem;
    Enregistrersous1: TMenuItem;
    N3: TMenuItem;
    Ouvrir1: TMenuItem;
    N7: TMenuItem;
    Exporter1: TMenuItem;
    N4: TMenuItem;
    Quitter1: TMenuItem;
    Barres1: TMenuItem;
    Nouvellebarre1: TMenuItem;
    Morceaux1: TMenuItem;
    Listedesmorceaux1: TMenuItem;
    N6: TMenuItem;
    Nouveaumorceau1: TMenuItem;
    Perages1: TMenuItem;
    Editerlalistedescodes1: TMenuItem;
    Profils1: TMenuItem;
    Editerlelistedescodes1: TMenuItem;
    N5: TMenuItem;
    Aproposde1: TMenuItem;
    ActionManager1: TActionManager;
    quitter: TAction;
    enregistrer: TAction;
    enregistrersous: TAction;
    ouvrir: TAction;
    exporter: TAction;
    nouvellebarre: TAction;
    listemorceaux: TAction;
    creermorceau: TAction;
    listecodespercages: TAction;
    listecodesprofiles: TAction;
    apropos: TAction;
    nouveau: TAction;
    Nouveau1: TMenuItem;
    N1: TMenuItem;
    ImageList1: TImageList;
    OpenDialog: TOpenDialog;
    importer: TAction;
    ImportDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    N2: TMenuItem;
    Importer1: TMenuItem;
    N8: TMenuItem;
    tempsfabrication: TAction;
    longueursprofiles: TAction;
    tempsfabrication1: TMenuItem;
    longueursprofiles1: TMenuItem;
    N10: TMenuItem;
    N9: TMenuItem;
    Aemporter1: TMenuItem;
    emporter: TAction;
    SaveDialog1: TSaveDialog;
    importermx: TAction;
    Importer2: TMenuItem;
    OpenDialog1: TOpenDialog;
    Rcents1: TMenuItem;
    Affichage1: TMenuItem;
    mvt: TAction;
    mhz: TAction;
    mcascade: TAction;
    Cascade1: TMenuItem;
    Mosaqueverticale1: TMenuItem;
    Mosaquehorizontale1: TMenuItem;
    N11: TMenuItem;
    procedure quitterExecute(Sender: TObject);
    procedure nouvellebarreExecute(Sender: TObject);
    procedure exporterExecute(Sender: TObject);
    procedure aproposExecute(Sender: TObject);
    procedure listecodespercagesExecute(Sender: TObject);
    procedure listecodesprofilesExecute(Sender: TObject);
    procedure listemorceauxExecute(Sender: TObject);
    procedure creermorceauExecute(Sender: TObject);
    procedure nouveauExecute(Sender: TObject);
    procedure ouvrirExecute(Sender: TObject);
    procedure importerExecute(Sender: TObject);
    procedure enregistrersousExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure enregistrerExecute(Sender: TObject);
    procedure tempsfabricationExecute(Sender: TObject);
    procedure longueursprofilesExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure opendirect;
    procedure openfile(filename: string);
    procedure FormShow(Sender: TObject);
    procedure emporterExecute(Sender: TObject);
    procedure importermxExecute(Sender: TObject);
    procedure mhzExecute(Sender: TObject);
    procedure mvtExecute(Sender: TObject);
    procedure mcascadeExecute(Sender: TObject);
    procedure Affichage1Click(Sender: TObject);
  private
    { Private declarations }
    recents: Tstringlist;
    procedure DroppedFiles(var msg: TWMDropFiles); message WM_DROPFILES;
    procedure WndProc(var msg: TMessage); override;
    procedure cl(Sender: TObject);
    procedure majRecents;
    procedure saveRecent(filename: string);
    procedure clmdi(Sender: TObject);
  public
    { Public declarations }
    activeform: Tform2;
  end;

const
  WM_PARAMETRE = WM_USER + 1;

var
  Form8: TForm8;

implementation

uses listeMorceauxFormU;
{$R *.dfm}

procedure TForm8.WndProc(var msg: TMessage);
var
  NumAtom: atom;
  Texte: PChar;
begin
  if (msg.msg = WM_PARAMETRE) then
  begin
    NumAtom := msg.LParam; // on r�cup�re l'adresse de l'Atom
    GetMem(Texte, 256); // on alloue 255 caract�res (MAX_PATH) + le caract�re 0
    GlobalGetAtomName(NumAtom, Texte, 256); // on remplit le tableau de caract�res
    GlobalDeleteAtom(NumAtom); // Surtout n'oubliez pas de lib�rer l'atom
    openfile(Texte);
    FreeMem(Texte); // on lib�re la m�moire allou�e au tableau de char
  end;
  inherited WndProc(msg); // si l'on veut que le message continue � se propager
end;

procedure TForm8.saveRecent(filename: string);
var
  I: Integer;
begin
  if recents.IndexOf(filename) < 0 then
    recents.insert(0, filename)
  else
    recents.Move(recents.IndexOf(filename), 0);
  I := recents.Count - 1;
  while I > 0 do
  begin
    if not fileexists(recents[I]) then
    begin
      recents.delete(I);
      I := recents.Count - 1;
    end;
    I := I - 1;
  end;
  if recents.Count > 10 then
    for I := recents.Count - 1 downto 10 do
      recents.delete(I);
  recents.saveTofile(extractfilepath(application.ExeName) + 'recents.txt');
  majRecents;
end;

procedure TForm8.openfile(filename: string);
begin
  try
    activeform := Tform2.Create(application);
    activeform.filename := filename;
    activeform.loadFile;
  except
    raise Exception.Create('Erreure lors de l''ouverture de: ' + filename);
  end;
  saveRecent(filename);
end;

// =Ouvre l'application directement sur le fichier depuis un double clic dans windows============================================================================
procedure TForm8.opendirect;
begin
  if ParamStr(1) <> '' then
  begin
    openfile(ParamStr(0));
  end;
end;

// =Ouvre les fichiers d�pos�s dans l'application============================================================================
procedure TForm8.DroppedFiles(var msg: TWMDropFiles);
var
  I, j: Integer;
  nombreDeFichiers: Integer;
  e, nomDuFichier: string;
  tailleDuBuffer: Integer;
  f: Tform2;
begin
  // R�cup�ration du nombre de fichiers d�pos�s sur la fen�tre
  nombreDeFichiers := DragQueryFile(msg.Drop, $FFFFFFFF, nil, 0);
  tailleDuBuffer := MAX_PATH;
  SetLength(nomDuFichier, tailleDuBuffer);
  for I := 0 to nombreDeFichiers - 1 do
  begin
    DragQueryFile(msg.Drop, I, PChar(nomDuFichier), tailleDuBuffer);
    e := trim(copy(nomDuFichier, pos('.', nomDuFichier) + 1, 4));
    if (e = 'cxe') or (e = 'csv') then
    begin
      for j := 0 to MDIChildCount - 1 do
      begin
        f := MDIChildren[j] as Tform2;
        if f.filename = nomDuFichier then
        begin
          f.BringToFront;
          exit;
        end;
      end;
      openfile(nomDuFichier);
    end
    else
      showmessage('L''application ne sais pas lire le fichier: ' + nomDuFichier);
  end;
end;

procedure TForm8.mvtExecute(Sender: TObject);
begin
  TileMode := tbVertical;
  Tile;
end;

procedure TForm8.mhzExecute(Sender: TObject);
begin
  TileMode := tbHorizontal;
  Tile;
end;

procedure TForm8.mcascadeExecute(Sender: TObject);
begin
  cascade;
end;

procedure TForm8.Affichage1Click(Sender: TObject);
var
  I: Integer;
  r: TMenuItem;
begin
  for I := Affichage1.Count - 1 downto 0 do
    if Affichage1[I].Tag >= 0 then
      Affichage1.delete(I);

  for I := 0 to MDIChildCount - 1 do
  begin
    r := TMenuItem.Create(application);
    r.Caption := MDIChildren[I].Caption;
    r.Tag := I;
    r.onClick := clmdi;
    r.Checked := MDIChildren[I] = ActiveMDIChild;
    Affichage1.Add(r);
  end;
end;

procedure TForm8.clmdi(Sender: TObject);
begin
  MDIChildren[TMenuItem(Sender).Tag].Show;
end;

procedure TForm8.aproposExecute(Sender: TObject);
var
  f9: Tform9;
begin
  f9 := Tform9.Create(application);
  f9.showmodal;
end;

procedure TForm8.creermorceauExecute(Sender: TObject);
var
  f: Tform5;
begin
  f := Tform5.Create(application);
  f.nouveau;
end;

procedure TForm8.emporterExecute(Sender: TObject);
begin
  (*
    if SaveDialog1.Execute then
    begin
    try
    // ===========================================================================
    // CREE LE ZIP DU CHANTIER COMPLET .EVRC======================================
    // ------------------
    FlexCompress1.filename := SaveDialog1.filename;
    // Create a new archive file
    FlexCompress1.OpenArchive();
    // Set path to folder with the files to archive
    FlexCompress1.BaseDir := extractfilepath(application.ExeName) + 'morceaux\';
    // vide l'archive
    FlexCompress1.deleteFiles('*.*');
    // Add all files and directories from the source folder to the archive
    FlexCompress1.AddFiles('*.*');
    FlexCompress1.CloseArchive();
    except
    FlexCompress1.CloseArchive();
    deletefile(SaveDialog1.filename);
    raise Exception.Create('Erreur lors de la cr�ation du zip ');
    exit;
    end;
    end;
    *)
end;

procedure TForm8.enregistrerExecute(Sender: TObject);
begin
  if (activeform.filename = '') then
  begin
    enregistrersousExecute(Sender);
    exit;
  end;
  activeform.saveFile;
  activeform.beSaved := FALSE;
  activeform.Caption := extractfilename(activeform.filename);
  saveRecent(activeform.filename);
end;

procedure TForm8.enregistrersousExecute(Sender: TObject);
begin
  if SaveDialog.Execute then
  begin
    activeform.filename := extractfilenameonly(SaveDialog.filename) + '.cxe';
    enregistrerExecute(Sender);
  end;
end;

procedure TForm8.exporterExecute(Sender: TObject);
begin
  activeform.exportStrings;
end;

procedure TForm8.FormCreate(Sender: TObject);
begin
  // D�finition de l'icone de l'application
  SetFormIcons(Handle, 'ICON_1', 'ICON_1');
  // accepte les fichiers d�pos�s
  DragAcceptFiles(self.Handle, True);
  //
  StatusBar1.Panels[0].Text := Caption + ' v' + getversion;
  //
  recents := Tstringlist.Create;
  if fileexists(extractfilepath(application.ExeName) + 'recents.txt') then
    recents.LoadFromFile(extractfilepath(application.ExeName) + 'recents.txt');
  majRecents;
end;

procedure TForm8.majRecents;
var
  r: TMenuItem;
  I: Integer;
begin
  Rcents1.Visible := recents.Count > 0;
  Rcents1.Clear;
  for I := 0 to recents.Count - 1 do
  begin
    r := TMenuItem.Create(application);
    r.Caption := extractfilenameonly(recents[I]);
    r.Tag := I;
    r.onClick := cl;
    Rcents1.Add(r);
  end;
end;

procedure TForm8.cl(Sender: TObject);
begin
  try
    openfile(recents[TMenuItem(Sender).Tag]);
  except

  end;
end;

procedure TForm8.FormDestroy(Sender: TObject);
begin
  DragAcceptFiles(self.Handle, FALSE);
  recents.Free;
end;

procedure TForm8.FormShow(Sender: TObject);
begin
  // ouvre le chantier que j'ai double cliqu� et qui � lanc� l'appli
  if ParamStr(1) <> '' then
    opendirect;
end;

procedure TForm8.importerExecute(Sender: TObject);
var
  t, l: Tstringlist;
  I, j: Integer;
  s: string;
  b: Tbarre;
  m: Tmorceau;
  lastcoupe: single;
begin
  if ImportDialog.Execute then
  begin
    activeform := Tform2.Create(application);
    t := Tstringlist.Create;
    l := Tstringlist.Create;
    t.LoadFromFile(ImportDialog.filename);
    j := 0;
    for I := 1 to t.Count - 1 do
    begin
      l.CommaText := t[I];
      // d�tection ligne barre
      if length(l[2]) = 18 then
      begin
        // showmessage('barre ('+t[i]+')');
        b := Tbarre.Create('Barre ' + l[0], strtoint(l[1]), '', strtoint(l[3]));
        activeform.ListBox1.Items.AddObject('Barre ' + l[0], b);
        continue;
      end;
      case strtoint(l[1]) of
        // coupe
        1:
          begin
            m := Tmorceau.Create;
            m.nom := 'Import ' + inttostr(j);
            m.longueur := trunc((strtoint(l[2]) + strtoint('0.' + l[3])) - lastcoupe - SCIE);
            lastcoupe := (strtoint(l[2]) + strtoint('0.' + l[3]));
            b.morceaux.AddObject(m.nom, m);
            // showmessage('coupe ('+t[i]+')');
            inc(j);
          end;
        // percage
        0:
          begin
            m.percages.Add(l[3] + '=' + l[2]);
            // showmessage('percage ('+t[i]+')');
          end;
      end;

    end;

  end;
end;

procedure TForm8.importermxExecute(Sender: TObject);
begin
  (*
    if OpenDialog1.Execute then
    begin
    try
    // ===========================================================================
    // OUVRE LE ZIP DU CHANTIER COMPLET .EVRC======================================
    FlexCompress1.filename := OpenDialog1.filename;
    // Create a new archive file
    FlexCompress1.OpenArchive();
    // Set path to the destination folder
    FlexCompress1.BaseDir := extractfilepath(application.ExeName) + 'morceaux\';
    // extract all files in archive
    FlexCompress1.ExtractFiles('*.*');
    // Close archive
    FlexCompress1.CloseArchive();
    except
    FlexCompress1.CloseArchive();
    showmessage('Erreure lors de l''importation des morceaux');
    end;
    end;
    *)
end;

procedure TForm8.listecodespercagesExecute(Sender: TObject);
var
  f6: Tform6;
begin
  f6 := Tform6.Create(application);
  f6.loadcodes(extractfilepath(application.ExeName) + 'codepercages.txt');
end;

procedure TForm8.listecodesprofilesExecute(Sender: TObject);
var
  f6: Tform6;
begin
  f6 := Tform6.Create(application);
  f6.loadcodes(extractfilepath(application.ExeName) + 'codeprofiles.txt');
end;

procedure TForm8.listemorceauxExecute(Sender: TObject);
begin
  form4.Button2.Visible := FALSE;
  form4.Button3.Visible := FALSE;
  form4.Show;
end;

procedure TForm8.longueursprofilesExecute(Sender: TObject);
var
  f6: Tform6;
begin
  f6 := Tform6.Create(application);
  f6.loadcodes(extractfilepath(application.ExeName) + 'longueurprofiles.txt');
end;

procedure TForm8.nouveauExecute(Sender: TObject);
begin
  activeform := Tform2.Create(application);
end;

procedure TForm8.nouvellebarreExecute(Sender: TObject);
var
  f1: Tform1;
begin
  f1 := Tform1.Create(application);
  f1.nouveau;
end;

procedure TForm8.ouvrirExecute(Sender: TObject);
begin
  if OpenDialog.Execute then
  begin
    openfile(OpenDialog.filename);
  end;
end;

procedure TForm8.quitterExecute(Sender: TObject);
begin
  close;
end;

procedure TForm8.tempsfabricationExecute(Sender: TObject);
var
  f6: Tform6;
begin
  f6 := Tform6.Create(application);
  f6.loadcodes(extractfilepath(application.ExeName) + 'temps.txt');
end;

procedure TForm8.Timer1Timer(Sender: TObject);
begin
  StatusBar1.Panels[1].Text := tailleo(GetHeapStatus.TotalAllocated);
end;

end.
