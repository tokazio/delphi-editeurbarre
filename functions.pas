unit functions;

interface

uses sysutils, Math, windows, shellapi, messages, dialogs, classes;

function tailleo(sz: extended): string;
function FileCount(const aFolder: string): Integer;
function GetIconHandle(const P_FileName: string; const P_Modifier: Integer): HIcon;
function getversion: string;
function SecondsToHMS(const aSec: LongWord): string;
Function ExtractFileNameOnly(FileName: TFileName): TFileName;
procedure SetFormIcons(FormHandle: HWND; SmallIconName, LargeIconName: string);
// function escapeFileName(const filename:string):string;
function adresse(value: pointer): string;
function IsValidFileName(const FileName: string): boolean;
function sortPercagesPosInc(List: TStringList; Index1, Index2: Integer): Integer;

implementation

function sortPercagesPosInc(List: TStringList; Index1, Index2: Integer): Integer;
begin
  Result := 0;
  if strtofloat(List.ValueFromIndex[Index1]) < strtofloat(List.ValueFromIndex[Index2]) then
    Result := -1;
  if strtofloat(List.ValueFromIndex[Index1]) > strtofloat(List.ValueFromIndex[Index2]) then
    Result := 1;
end;

function adresse(value: pointer): string;
begin
  Result := Format('%p', [value]);
end;

function IsValidFileName(const FileName: string): boolean;
const
  InvalidCharacters: set of char = ['\', '/', ':', '*', '?', '"', '<', '>', '|'];
var
  c: char;
begin
  Result := FileName <> '';
  if Result then
  begin
    for c in FileName do
    begin
      Result := NOT(c in InvalidCharacters);
      if NOT Result then
        break;
    end;
  end;
end;

(* IsValidFileName *)
(*
  function escapeFileName(const filename:string):string;
  var
  i: integer;
  ResultWithSpaces: string;
  begin

  ResultWithSpaces := filename;

  for i := 1 to Length(ResultWithSpaces) do
  begin
  // These chars are invalid in file names.
  case ResultWithSpaces[i] of
  '/', '\', ':', '*', '?', '"', '|', ' ', #$D, #$A, #9:
  // Use a * to indicate a duplicate space so we can remove
  // them at the end.
  {$WARNINGS OFF} // W1047 Unsafe code 'String index to var param'
  if (i > 1) and
  ((ResultWithSpaces[i - 1] = ' ') or (ResultWithSpaces[i - 1] = '*')) then
  ResultWithSpaces[i] := '*'
  else
  ResultWithSpaces[i] := ' ';

  {$WARNINGS ON}
  end;
  end;

  // A * indicates duplicate spaces.  Remove them.
  result := stringReplace(ResultWithSpaces, '*', '',[rfReplaceAll, rfIgnoreCase]);

  // Also trim any leading or trailing spaces
  result := Trim(Result);

  if result = '' then
  begin
  raise(Exception.Create('Resulting FileName was empty'+#13+'Input string was: '
  + filename));
  end;
  end;
*)
procedure SetFormIcons(FormHandle: HWND; SmallIconName, LargeIconName: string);
var
  hIconS, hIconL: Integer;
begin
  hIconS := LoadIcon(hInstance, PChar(SmallIconName));
  if hIconS > 0 then
  begin
    hIconS := SendMessage(FormHandle, WM_SETICON, ICON_SMALL, hIconS);
    if hIconS > 0 then
      DestroyIcon(hIconS);
  end;
  hIconL := LoadIcon(hInstance, PChar(LargeIconName));
  if hIconL > 0 then
  begin
    hIconL := SendMessage(FormHandle, WM_SETICON, ICON_BIG, hIconL);
    if hIconL > 0 then
      DestroyIcon(hIconL);
  end;
end;

Function ExtractFileNameOnly(FileName: TFileName): TFileName;
var
  ExtPart: TFileName;
  lgExt: Integer;
begin
  ExtPart := ExtractFileExt(FileName);
  lgExt := Length(ExtPart);
  Delete(FileName, Length(FileName) - lgExt + 1, lgExt);
  Result := FileName;
end;

function SecondsToHMS(const aSec: LongWord): string;
var
  vH, vM, vS: word;
  H, M, S: string;
begin
  vS := aSec mod 60;
  if vS < 10 then
    S := '0' + inttostr(vS)
  else
    S := inttostr(vS);
  vM := (aSec div 60) mod 60;
  if vM < 10 then
    M := '0' + inttostr(vM)
  else
    M := inttostr(vM);
  vH := aSec div 3600;
  if vH < 10 then
    H := '0' + inttostr(vH)
  else
    H := inttostr(vH);

  Result := H + ':' + M + ':' + S;
end;

function getversion: string;
var
  VerInfoSize, VerValueSize, Dummy: DWord;
  VerInfo: pointer;
  VerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  { Deux solutions : }
  if VerInfoSize <> 0 then
  { - Les info de version sont inclues }
  begin
    { On alloue de la m�moire pour un pointeur sur les info de version : }
    GetMem(VerInfo, VerInfoSize);
    { On r�cup�re ces informations : }
    GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
    VerQueryValue(VerInfo, '\', pointer(VerValue), VerValueSize);
    { On traite les informations ainsi r�cup�r�es : }
    with VerValue^ do
    begin
      Result := inttostr(dwFileVersionMS shr 16);
      Result := Result + '.' + inttostr(dwFileVersionMS and $FFFF);
      Result := Result + '.' + inttostr(dwFileVersionLS shr 16);
      Result := Result + '.' + inttostr(dwFileVersionLS and $FFFF);
    end;

    { On lib�re la place pr�c�demment allou�e : }
    FreeMem(VerInfo, VerInfoSize);
  end

  else
    { - Les infos de version ne sont pas inclues }
    { On d�clenche une exception dans le programme : }
    raise EAccessViolation.Create('Les informations de version ne sont pas inclues');

end;

(*
  Image1.Picture.Icon.Handle := GetIconHandle(FileName, SHGFI_LINKOVERLAY);
  Image2.Picture.Icon.Handle := GetIconHandle(FileName, SHGFI_SELECTED);
  Image3.Picture.Icon.Handle := GetIconHandle(FileName, SHGFI_OPENICON);
  Image4.Picture.Icon.Handle := GetIconHandle(FileName, SHGFI_SHELLICONSIZE);
  Image5.Picture.Icon.Handle := GetIconHandle(FileName, SHGFI_SMALLICON);
  Image6.Picture.Icon.Handle := GetIconHandle(FileName, SHGFI_SMALLICON + SHGFI_LINKOVERLAY)
*)
function GetIconHandle(const P_FileName: string; const P_Modifier: Integer): HIcon;
var
  Sfi: TSHFileInfo;
begin
  SHGetFileInfo(PChar(P_FileName), 0, Sfi, SizeOf(TSHFileInfo), SHGFI_SYSICONINDEX or SHGFI_ICON or P_Modifier);
  Result := Sfi.HIcon;
end;

function FileCount(const aFolder: string): Integer;
var
  H: THandle;
  Data: TWin32FindData;
begin
  Result := 0;
  H := FindFirstFile(PChar(aFolder + '*.*'), Data);
  if H <> INVALID_HANDLE_VALUE then
    repeat
      Inc(Result, Ord(Data.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY = 0));
    until not FindNextFile(H, Data);
    windows.FindClose(H);
end;

function tailleo(sz: extended): string;
begin
  If sz < 1024 Then
    Result := floattostr(sz) + 'o'
  Else If (sz >= 1024) And (sz < power(1024, 2)) Then
    Result := formatfloat('#.00', sz / 1024) + ' Ko'
  Else If (sz >= power(1024, 2)) And (sz < power(1024, 3)) Then
    Result := formatfloat('#.00', (sz / power(1024, 2))) + ' Mo'
  Else If (sz >= power(1024, 3)) And (sz < power(1024, 4)) Then
    Result := formatfloat('#.00', (sz / power(1024, 3))) + ' Go';
end;

end.
