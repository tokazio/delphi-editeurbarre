unit creerBarreFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, barresU;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    ComboBox1: TComboBox;
    Button1: TButton;
    Button2: TButton;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Memo1: TMemo;
    Label4: TLabel;
    Edit2: TEdit;
    Label5: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure nouveau;
    procedure edit(barre:Tbarre);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Flgs:Tstringlist;
    Faction:Tactions;
    Fbarre:Tbarre;
    function valid: boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses projetFormU, mainFormU;

{$R *.dfm}

procedure Tform1.edit(barre:Tbarre);
begin
  if barre=nil then exit;
  Fbarre:=barre;
  caption:='Editer la barre';
  edit1.text:=Fbarre.nom;
  edit2.Text:=inttostr(Fbarre.longueur);
  combobox1.itemindex:=Fbarre.codeprofil;
  memo1.lines:=Fbarre.description;
  Faction:=aEdit;
  showmodal;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  valid;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  valid;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action:=cafree;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  combobox1.Items.LoadFromFile(extractfilepath(application.ExeName)+'codeprofiles.txt');
  Flgs:=Tstringlist.Create;
  Flgs.LoadFromFile(extractfilepath(application.ExeName)+'longueurprofiles.txt');
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Flgs.Free;
end;

function TForm1.valid:boolean;
begin
  button1.enabled:=(edit1.text<>'') and (combobox1.itemindex>0) and (edit2.text<>'');
  Result:=button1.Enabled;
end;

procedure Tform1.nouveau;
begin
  caption:='Cr�er une barre';
  edit1.Text:='';
  edit2.Text:='';
  combobox1.text:='Choisir...';

  edit1.Text:='Barre '+inttostr(form8.activeform.ListBox1.Count+1);

  memo1.Clear;
  Faction:=aAdd;
  showmodal;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i:integer;
begin
  if edit1.Text='' then exit;
  if edit2.Text='' then exit;
  if combobox1.ItemIndex<1 then exit;

  case Faction of
    aNone: ;
    aAdd: begin
      i:=form8.activeform.listbox1.Items.AddObject(edit1.text,Tbarre.create(edit1.Text,combobox1.ItemIndex,memo1.Lines.Text,strtoint(edit2.Text)));
      form8.activeform.ListBox1.ItemIndex:=i;
    end;
    aEdit: begin
      Fbarre.nom:=Edit1.Text;
      Fbarre.longueur:=strtoint(Edit2.Text);
      Fbarre.codeprofil:=combobox1.itemindex;
      Fbarre.description:=memo1.lines;
    end;
  end;
  form8.activeform.ListBox1Click(sender);
  form8.activeform.beSaved:=TRUE;
  close;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  edit2.Text:=Flgs.Values[inttostr(combobox1.ItemIndex)];
end;

end.
