unit listeMorceauxFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, barresU, Menus, projetFormU, functions,creerMorceauFormU,duppliquerMorceauFormU;

type
  TForm4 = class(TForm)
    ListView1: TListView;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    PopupMenu1: TPopupMenu;
    Editer1: TMenuItem;
    Supprimer1: TMenuItem;
    N1: TMenuItem;
    duppliquer1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Editer1Click(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Supprimer1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure duppliquer1Click(Sender: TObject);
  private
    { Private declarations }
    FcodeProfils: Tstringlist;
    procedure majlistMorceaux(Sender: TObject);
    procedure clear;
  public
    { Public declarations }
    ff: Tform2;
  end;

var
  Form4: TForm4;

implementation

uses mainFormU;
{$R *.dfm}

procedure TForm4.majlistMorceaux(Sender: TObject);
Var
  Chemin: String;
  Info: TSearchRec;
  it: Tlistitem;
  m: Tmorceau;
begin
  clear;
  ListView1.Items.beginUpdate;
  { Pour �tre sur que la barre oblique finisse le nom du chemin }
  Chemin := extractfilepath(application.ExeName) + 'morceaux/';
  { Recherche de la premi�re entr�e du r�pertoire }
  If FindFirst(Chemin + '*.*', faAnyFile, Info) = 0 Then
  Begin
    Repeat
      If (Info.Attr And faDirectory) = 0 Then
      begin
        it := ListView1.Items.Add;
        // it.Caption:=Chemin+Info.FindData.cFileName;
        try
          m := Tmorceau.create(Chemin + Info.FindData.cFileName);
        except
          showmessage('Liste des morceaux::Erreur lors de la cr�ation du morceau avec le fichier: ' + Chemin + Info.FindData.cFileName);
          it.Delete;
          continue;
        end;
        it.Caption := m.nom;
        // it.SubItems.Add(m.description.DelimitedText);
        it.SubItems.Add(floattostr(m.longueur));
        it.Data := m;
        if m.gamme = 'Chrome' then
          it.groupid := 0;
        if m.gamme = 'Baludesign' then
          it.groupid := 1;
        if m.gamme = 'Monoprofil' then
          it.groupid := 2;

      end;
      { Il faut ensuite rechercher l'entr�e suivante }
    Until FindNext(Info) <> 0;
    { Dans le cas ou une entr�e au moins est trouv�e il faut }
    { appeler FindClose pour lib�rer les ressources de la recherche }
    FindClose(Info);
  End;
  ListView1.Items.endUpdate;
end;

procedure TForm4.PopupMenu1Popup(Sender: TObject);
begin
  Editer1.Enabled := ListView1.Selected <> nil;
  duppliquer1.Enabled := ListView1.Selected <> nil;
  Supprimer1.Enabled := ListView1.Selected <> nil;
end;

procedure TForm4.Supprimer1Click(Sender: TObject);
begin
  if ListView1.Selected.Data = nil then
    exit;
  if pos('STD', Tmorceau(ListView1.Selected.Data).nom) > 0 then
  begin
    showmessage('Impossible de supprimer un morceau standard (STD)');
  end
  else
  begin
    case MessageBox(0, PwideCHar('Effacer d�finitivement le morceau "' + Tmorceau(ListView1.Selected.Data).nom + '" ?'), 'Effacer', +mb_YesNo + mb_ICONWARNING) of
      // yes
      6:
        begin
          deletefile(Tmorceau(ListView1.Selected.Data).filename);
          majlistMorceaux(Sender);
        end;
      // no
      7:
        begin

        end;
    end;
  end;
end;

procedure TForm4.Button1Click(Sender: TObject);
var
  f: Tform5;
begin
  f := Tform5.create(application);
  // f.callBack:=majlistMorceaux;
  f.nouveau;
  majlistMorceaux(Sender);
end;

procedure TForm4.Button2Click(Sender: TObject);
var
  m: Tmorceau;
  id: integer;
begin
  if ff = nil then
    exit;
  if ff.barreactive = nil then
    exit;

  try
    m := Tmorceau.create;
    m.assign(Tmorceau(ListView1.Selected.Data));
  except
    raise Exception.create('Impossible de cr�er le morceau!');
    m.free;
    exit;
  end;

  if m.codeprofil > 0 then
    if ff.barreactive.codeprofil <> m.codeprofil then
    begin
      if (m.codeprofil > FcodeProfils.Count) then
      begin
        showmessage('Impossible d''ajouter le morceau' + #13 + 'Le code profil du morceau n''existe pas! (code ' + inttostr(m.codeprofil) + ').' + #13 + 'La barre est en profil�: ' + FcodeProfils[ff.barreactive.codeprofil]);
      end
      else
      begin
        showmessage('Impossible d''ajouter le morceau' + #13 + 'Ce morceau doit �tre usin� dans le profil�: ' + #13 + FcodeProfils[m.codeprofil] + #13 + ' et la barre est en profil�: ' + #13 + FcodeProfils[ff.barreactive.codeprofil]);
      end;
      m.free;
      exit;
    end;

  if m.longueur > ff.longueurDispo then
  begin
    m.free;
    exit;
  end;
  try
    id := ff.barreactive.morceaux.AddObject(m.nom, m);
  except
    raise Exception.create('Tform4.Button2Click' + #13 + 'Impossible d''ajouter le morceau � la barre!');
    m.free;
  end;
  ff.majlistbarre(id);
  ff.beSaved := TRUE;
end;

procedure TForm4.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TForm4.Editer1Click(Sender: TObject);
var
  f: Tform5;
begin
  if ListView1.Selected.Data = nil then
    exit;
  if pos('STD', Tmorceau(ListView1.Selected.Data).nom) > 0 then
  begin
    showmessage('Impossible d''�diter un morceau standard (STD)');
  end
  else
  begin
    f := Tform5.create(application);
    // f.callBack:=majlistMorceaux;
    f.edit(Tmorceau(ListView1.Selected.Data));
    majlistMorceaux(Sender);
  end;
end;

procedure TForm4.clear;
var
  I: integer;
begin
  ListView1.Items.beginUpdate;
  for I := 0 to ListView1.Items.Count - 1 do
  begin
    Tmorceau(Tlistitem(ListView1.Items[I]).Data).free;
    ListView1.Items[I].Data := nil;
  end;
  ListView1.clear;
  ListView1.Items.endUpdate;
end;

procedure TForm4.duppliquer1Click(Sender: TObject);
var
  f: string;
  l: Tstringlist;
  f11:Tform11;
begin
  if ListView1.Selected.Data = nil then
    exit;
  f11:=Tform11.Create(application);
  f11.dupp(Tmorceau(ListView1.Selected.Data).nom);
  if f11.showModal = mrOk then
  begin
    f := extractfilepath(application.ExeName);
    CopyFile(PChar(f + Tmorceau(ListView1.Selected.Data).filename), PChar(f + '\Morceaux\' + f11.Edit1.Text + '.txt'), TRUE);
    l := Tstringlist.create;
    l.NameValueSeparator := '>';
    l.LoadFromFile(f + '\Morceaux\' + f11.Edit1.Text + '.txt');
    l.Values['nom'] := f11.Edit1.Text;
    l.saveToFile(f + '\Morceaux\' + f11.Edit1.Text + '.txt');
    l.free;
    Form4.majlistMorceaux(Sender);
  end;
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
  FcodeProfils := Tstringlist.create;
  FcodeProfils.LoadFromFile(extractfilepath(application.ExeName) + 'codeprofiles.txt');
end;

procedure TForm4.FormDestroy(Sender: TObject);
begin
  FcodeProfils.free;
  clear;
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  majlistMorceaux(Sender);
end;

procedure TForm4.ListView1Click(Sender: TObject);
begin
  Button2.Enabled := ListView1.Selected <> nil;
end;

procedure TForm4.ListView1DblClick(Sender: TObject);
begin
  if ListView1.Selected = nil then
    exit;
  Button2Click(Sender);
end;

end.
