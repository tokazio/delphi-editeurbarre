object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Choisir un morceau'
  ClientHeight = 573
  ClientWidth = 596
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    596
    573)
  PixelsPerInch = 96
  TextHeight = 13
  object ListView1: TListView
    Left = 0
    Top = 0
    Width = 596
    Height = 533
    Hint = 
      'Double cliquez sur un morceau pour l'#39'ajouter '#224' la barre en cours' +
      ' d'#39#233'dition'
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        AutoSize = True
        Caption = 'Nom'
      end
      item
        AutoSize = True
        Caption = 'Longueur'
      end>
    ColumnClick = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    GridLines = True
    Groups = <
      item
        Header = 'Chrome'
        GroupID = 0
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        TitleImage = -1
        ExtendedImage = -1
      end
      item
        Header = 'Baludesign'
        GroupID = 1
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        TitleImage = -1
        ExtendedImage = -1
      end
      item
        Header = 'Monoprofil'
        GroupID = 2
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        TitleImage = -1
        ExtendedImage = -1
      end>
    GroupView = True
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ShowHint = True
    TabOrder = 0
    ViewStyle = vsReport
    OnClick = ListView1Click
    OnDblClick = ListView1DblClick
  end
  object Button2: TButton
    Left = 447
    Top = 540
    Width = 141
    Height = 25
    Hint = 'Ajouter le morceau choisis '#224' la barre en cours d'#39#233'dition'
    Anchors = [akRight, akBottom]
    Caption = 'Ajouter '#224' la barre'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 363
    Top = 540
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Fermer'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button1: TButton
    Left = 10
    Top = 540
    Width = 31
    Height = 25
    Hint = 'Nouveau morceau'
    Anchors = [akLeft, akBottom]
    Caption = '+'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = Button1Click
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 290
    Top = 204
    object Editer1: TMenuItem
      Caption = 'Editer le morceau'
      OnClick = Editer1Click
    end
    object duppliquer1: TMenuItem
      Caption = 'Duppliquer...'
      OnClick = duppliquer1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Supprimer1: TMenuItem
      Caption = 'Effacer le morceau'
      OnClick = Supprimer1Click
    end
  end
end
