unit projetFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, barresU, Menus, functions, creerBarreFormU, creerMorceauFormU, Unit6, exportFormU;

type

  Tcube = class
  Private
    Frect: Trect;
    Factive: boolean;
    Fid: integer;
  Published
    property id: integer read Fid;
  Public
    constructor create(r: Trect; id: integer);
    procedure draw(canvas: Tcanvas);
    procedure active;
    procedure deactive;
    function clic(x, y: integer): boolean;
  end;

  TForm2 = class(TForm)
    ListView1: TListView;
    Button1: TButton;
    ListBox1: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PopupMenu1: TPopupMenu;
    Editer1: TMenuItem;
    N1: TMenuItem;
    Supprimer1: TMenuItem;
    PopupMenu2: TPopupMenu;
    Editer2: TMenuItem;
    N2: TMenuItem;
    Supprimer2: TMenuItem;
    Panel5: TPanel;
    Label4: TLabel;
    Button3: TButton;
    Button4: TButton;
    Label5: TLabel;
    N8: TMenuItem;
    Dupliquer1: TMenuItem;
    Button5: TButton;
    Button6: TButton;
    Panel6: TPanel;
    PaintBox1: TPaintBox;
    Panel7: TPanel;
    Panel8: TPanel;
    PaintBox2: TPaintBox;
    Splitter2: TSplitter;
    Button7: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure Editer1Click(Sender: TObject);
    procedure majlistbarre(id: integer);
    procedure Dupliquer1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Supprimer2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Supprimer1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Editer2Click(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
    procedure ListView1Change(Sender: TObject; Item: TListItem; Change: TItemChange);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
    procedure FormResize(Sender: TObject);
    procedure PaintBox2Paint(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
    Ftitre: string;
    Ffilename: string;
    FbeSaved: boolean;
    procedure setfilename(value: string);
    procedure setTitre(value: string);
    procedure setBeSaved(value: boolean);
    procedure setCaption;
    procedure alerteChute(recalcul: boolean);
    procedure clearCubes;
    procedure calcCubes;
    procedure deactiveCubes;
    procedure activeCube(id: integer);
    function coteV(c: Tcanvas; yStart, yEnd: single; xAxe, xPos: integer; text: string): integer;
  Published
    property filename: string read Ffilename write setfilename;
    property titre: string read Ftitre write setTitre;
    property beSaved: boolean read FbeSaved write setBeSaved;
  public
    { Public declarations }
    longueurDispo: single;
    temp, tottemp: integer;
    barreactive: Tbarre;
    morceauactif: Tmorceau;
    cubes: Tlist;
    procedure loadFile;
    procedure saveFile;
    procedure exportStrings;
  end;

const
  SCIE: integer = 5;

var
  Form2: TForm2;

implementation

uses mainFormU, listeMorceauxFormU, ajoutLongueurFormU;
{$R *.dfm}

procedure TForm2.setBeSaved(value: boolean);
begin
  FbeSaved := value;
  setCaption;
end;

procedure TForm2.saveFile;
var
  I, J: integer;
  tmp: Tstringlist;
begin
  (*
    if not directoryexists(extractfilepath(application.exename)+'tmp\') then mkdir(extractfilepath(application.exename)+'tmp\');
    *)

  tmp := Tstringlist.create;
  for I := 0 to ListBox1.Count - 1 do
  begin
    tmp.Add(Tbarre(ListBox1.Items.Objects[I]).nom + '>' + Tbarre(ListBox1.Items.Objects[I]).toFileString);
  end;
  // tmp.SaveToFile(extractfilepath(application.exename)+'tmp\'+extractfilename(filename));
  tmp.SaveToFile(filename);
  tmp.Free;
  (*
    try
    //===========================================================================
    //CREE LE ZIP DU CHANTIER COMPLET .EVRC======================================
    //------------------
    form8.flexCompress1.FileName:=filename;
    // Create a new archive file
    form8.flexCompress1.OpenArchive();
    // Set path to folder with the files to archive
    form8.flexCompress1.BaseDir:=extractfilepath(application.ExeName)+'morceaux\';
    //vide l'archive
    form8.flexcompress1.deleteFiles('*.*');
    // Ajoute les morceaux utilis�s
    for I := 0 to Listbox1.Count - 1 do
    begin
    for J:=0 to Tbarre(listbox1.Items.Objects[i]).morceaux.Count-1 do
    begin
    form8.flexCompress1.AddFiles(extractfilepath(application.exename)+'morceaux\'+Tmorceau(Tbarre(listbox1.Items.Objects[i]).morceaux.Objects[j]).nom+'.txt');
    end;
    end;
    form8.flexCompress1.AddFiles(extractfilepath(application.exename)+'tmp\'+extractfilename(filename));

    form8.flexCompress1.CloseArchive();
    except
    form8.flexCompress1.CloseArchive();
    deletefile(filename);
    raise Exception.Create('Erreur lors de l''enregistrement en zip du fichier '+filename);
    exit;
    end;
    *)
end;

procedure TForm2.Supprimer1Click(Sender: TObject);
var
  id: integer;
begin
  if MessageBox(0, 'Etes vous s�r de vouloir supprimer la barre?', 'Confirmation', +mb_YesNo + mb_ICONQUESTION) = 6 then
  begin
    id := ListBox1.itemindex - 1;
    ListBox1.Items.Objects[ListBox1.itemindex].Free;
    ListBox1.Items.Objects[ListBox1.itemindex] := nil;
    ListBox1.DeleteSelected;
    if ListBox1.itemindex > 0 then
    begin
      ListBox1.itemindex := id;
      ListBox1Click(nil);
    end
    else
    begin
      barreactive := nil;
      morceauactif := nil;
    end;
  end;
end;

procedure TForm2.Supprimer2Click(Sender: TObject);
var
  b: Tbarre;
  id: integer;
begin
  if MessageBox(0, 'Etes vous s�r de vouloir supprimer le morceau?', 'Confirmation', +mb_YesNo + mb_ICONQUESTION) = 6 then
  begin
    id := ListView1.itemindex;
    b := Tbarre(ListBox1.Items.Objects[ListBox1.itemindex]);
    Tmorceau(b.morceaux.Objects[ListView1.itemindex]).Free;
    b.morceaux.Objects[ListView1.itemindex] := nil;
    b.morceaux.Delete(ListView1.itemindex);
    ListView1.DeleteSelected;
    beSaved := TRUE;
    calcCubes;
    PaintBox1.Repaint;
    ListView1.itemindex := id - 1;
    ListView1SelectItem(Sender, ListView1.Selected, TRUE);
    alerteChute(TRUE);
  end;
end;

procedure TForm2.loadFile;
var
  tmp, btmp: Tstringlist;
  I: integer;
  b: Tbarre;
begin
  (*
    try
    //===========================================================================
    //OUVRE LE ZIP DU CHANTIER COMPLET .EVRC======================================
    form8.flexCompress1.FileName:=filename;
    // Create a new archive file
    form8.flexCompress1.OpenArchive();
    // Set path to the destination folder
    form8.flexCompress1.BaseDir:= extractfilepath(application.ExeName)+'morceaux\';
    // extract all files in archive
    form8.flexCompress1.ExtractFiles('*.txt');
    // Set path to the destination folder
    form8.flexCompress1.BaseDir:= extractfilepath(application.ExeName)+'tmp\';
    // extract all files in archive
    form8.flexCompress1.ExtractFiles(extractfilename(filename));
    // Close archive
    form8.flexCompress1.CloseArchive();
    except
    form8.flexCompress1.CloseArchive();
    showmessage('Erreure lors de l''ouverture ZIP du fichier '+filename);
    exit;
    end;
    *)
  self.filename := filename;
  tmp := Tstringlist.create;
  tmp.NameValueSeparator := '>';
  tmp.loadfromfile(filename);
  ListBox1.Items.beginupdate;
  ListBox1.clear;
  for I := 0 to tmp.Count - 1 do
  begin
    btmp := Tstringlist.create;
    btmp.Delimiter := '@';
    btmp.DelimitedText := tmp.ValueFromIndex[I];

    b := Tbarre.create(btmp.Values['nom'], strtoint(btmp.Values['codeprofil']), btmp.Values['description'], strtoint(btmp.Values['longueur']));
    b.openMorceaux(btmp.Values['morceaux']);
    // ajoute la barre � la liste
    ListBox1.Items.AddObject(tmp.Names[I], b);

    btmp.Free;
  end;
  ListBox1.Items.endupdate;
  tmp.Free;
  beSaved := False;
end;

procedure TForm2.exportStrings;
var
  I: integer;
  barre: Tbarre;
  f7: Tform7;
begin
  f7 := Tform7.create(application);
  f7.Memo1.clear;
  f7.Memo1.Lines.Add('//Cr�� avec ' + form8.Caption + ' v' + getversion + ' le ' + datetostr(now));
  tottemp := 0;
  for I := 0 to ListBox1.Count - 1 do
  begin
    barre := Tbarre(ListBox1.Items.Objects[I]);
    // try
    barre.exportStrings(f7.Memo1.Lines, I + 1);
    // except
    // showmessage('Erreure pour l''export de "' + barre.nom + '"');
    // end;
    tottemp := tottemp + temp;
  end;
  f7.Label2.Caption := SecondsToHMS(tottemp);
  f7.show;
end;

procedure TForm2.majlistbarre(id: integer);
var
  I: integer;
  tailletot, coeff, posx: single;
  it: TListItem;
begin
  if ListBox1.itemindex < 0 then
    barreactive := nil;
  if barreactive = nil then
  begin
    raise Exception.create('Tform2.majlistbarre' + #13 + 'Pas de barre avec laquelle mettre � jour la liste des morceaux!');
    // close;
    exit;
  end;
  coeff := (PaintBox1.Width - 6) / barreactive.longueur;
  ListView1.Items.beginupdate;
  ListView1.Items.clear;
  longueurDispo := 0;
  tailletot := 0;
  posx := 3;
  if barreactive.morceaux.Count > 0 then
  begin
    for I := 0 to barreactive.morceaux.Count - 1 do
    begin
      it := ListView1.Items.Add;
      it.Caption := Tmorceau(barreactive.morceaux.Objects[I]).nom;
      tailletot := tailletot + Tmorceau(barreactive.morceaux.Objects[I]).longueur + SCIE;
      it.Data := Tmorceau(barreactive.morceaux.Objects[I]);
    end;
  end;
  ListView1.Items.endupdate;
  alerteChute(TRUE);
  calcCubes;
  if id >= 0 then
  begin
    ListView1.itemindex := id;
    ListView1SelectItem(nil, ListView1.Selected, TRUE);
  end;
  PaintBox1.Repaint;
end;

procedure TForm2.alerteChute(recalcul: boolean);
var
  I: integer;
  tailletot: single;
begin
  if barreactive = nil then
  begin
    Panel7.visible := False;
    exit;
  end;
  if recalcul then
  begin
    longueurDispo := 0;
    tailletot := 0;
    for I := 0 to barreactive.morceaux.Count - 1 do
    begin
      tailletot := tailletot + Tmorceau(barreactive.morceaux.Objects[I]).longueur + SCIE;
    end;
    longueurDispo := barreactive.longueur - tailletot - SCIE;
  end;
  Label4.Caption := inttostr(ListView1.Items.Count) + ' morceau(x)';
  Label5.Caption := 'Longueur utilis� dans la barre: ' + formatfloat('#,00', tailletot) + 'mm (reste ' + formatfloat('#.00', longueurDispo) + 'mm)';
  // button2.Enabled:=tailletot<barreactive.longueur-SCIE;
  Panel7.visible := longueurDispo < 30;
end;

procedure TForm2.FormActivate(Sender: TObject);
begin
  with form8 do
  begin
    enregistrer.enabled := TRUE;
    enregistrersous.enabled := TRUE;
    exporter.enabled := TRUE;
    nouvellebarre.enabled := TRUE;
    // ---
    activeform := self;
  end;
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with form8 do
  begin
    enregistrer.enabled := False;
    enregistrersous.enabled := False;
    exporter.enabled := False;
    nouvellebarre.enabled := False;
    // ---
    activeform := nil;
  end;
  Action := caFree;
end;

procedure TForm2.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  if FbeSaved then
  begin
    case MessageBox(0, PwideCHar('Enregistrer "' + titre + '" avant de quitter?'), 'Modifications', +mb_YesNoCancel + mb_ICONWARNING) of
      // yes
      6:
        begin
          form8.enregistrerExecute(Sender);
          CanClose := TRUE;
        end;
      // cancel
      2:
        begin
          CanClose := False;
        end;
      // no
      7:
        begin
          CanClose := TRUE;
        end;
    end;
  end;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  Button1.Caption := '+';
  titre := 'Nouveau ' + inttostr(form8.MDIChildCount);
  beSaved := TRUE;
  cubes := Tlist.create;
end;

procedure TForm2.FormDestroy(Sender: TObject);
var
  I: integer;
begin
  for I := 0 to ListBox1.Count - 1 do
  begin
    Tbarre(ListBox1.Items.Objects[I]).Free;
    ListBox1.Items.Objects[I] := nil;
  end;
  ListBox1.clear;
  clearCubes;
  cubes.Free;
end;

procedure TForm2.FormResize(Sender: TObject);
begin
  if assigned(cubes) then
    calcCubes;
end;

procedure TForm2.clearCubes();
var
  I: integer;
begin
  for I := 0 to cubes.Count - 1 do
  begin
    Tcube(cubes[I]).Free;
    cubes[I] := nil;
  end;
  cubes.clear;
end;

procedure TForm2.deactiveCubes();
var
  I: integer;
begin
  for I := 0 to cubes.Count - 1 do
    Tcube(cubes[I]).deactive;
end;

procedure TForm2.activeCube(id: integer);
var
  I: integer;
begin
  for I := 0 to cubes.Count - 1 do
    if Tcube(cubes[I]).id = id then
    begin
      Tcube(cubes[I]).active;
      exit;
    end;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  form4.show; // addmorceau(self);
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  Supprimer1Click(Sender);
end;

procedure TForm2.Button4Click(Sender: TObject);
begin
  Supprimer2Click(Sender);
end;

procedure TForm2.Button5Click(Sender: TObject);
var
  b: Tbarre;
  I: integer;
begin
  b := Tbarre(ListBox1.Items.Objects[ListBox1.itemindex]);
  //
  I := ListView1.Selected.Index + 1;
  if I > ListView1.Items.Count - 1 then
    exit;

  // vers le bas
  b.morceaux.Exchange(ListView1.Selected.Index, I);
  //
  majlistbarre(-1);
  //
  calcCubes;
  //
  ListView1.itemindex := I;
  //
  beSaved := TRUE;
  PaintBox1.Repaint;
end;

procedure TForm2.Button6Click(Sender: TObject);
var
  b: Tbarre;
  I: integer;
begin
  b := Tbarre(ListBox1.Items.Objects[ListBox1.itemindex]);
  //
  I := ListView1.Selected.Index - 1;
  if I < 0 then
    exit;

  // vers le haut
  b.morceaux.Exchange(ListView1.Selected.Index, I);
  //
  majlistbarre(-1);
  //
  calcCubes;
  //
  ListView1.itemindex := I;
  //
  beSaved := TRUE;
  PaintBox1.Repaint;
end;

procedure TForm2.Button7Click(Sender: TObject);
var
  f10: Tform10;
begin
  f10 := Tform10.create(application);
  f10.ff := self;
  f10.show;
end;

procedure TForm2.Dupliquer1Click(Sender: TObject);
begin
  (*
    Tbarre(listbox1.Items.Objects[listbox1.ItemIndex]).duplicate(Tmorceau(listview1.Selected.Data));
    majlistbarre(Tbarre(listbox1.Items.Objects[listbox1.ItemIndex]));
    *)
  beSaved := TRUE;
end;

procedure TForm2.Editer1Click(Sender: TObject);
begin
  form1.Edit(Tbarre(ListBox1.Items.Objects[ListBox1.itemindex]));
end;

procedure TForm2.Editer2Click(Sender: TObject);
var
  f: Tform5;
begin
  if ListView1.Selected.Data = nil then
    exit;
  f := Tform5.create(application);
  f.Edit(Tmorceau(ListView1.Selected.Data));
  beSaved := TRUE;
  majlistbarre(-1);
end;

procedure TForm2.ListBox1Click(Sender: TObject);
begin
  if ListBox1.itemindex < 0 then
  begin
    Button2.enabled := False;
    Button3.enabled := False;
    exit;
  end;
  barreactive := Tbarre(ListBox1.Items.Objects[ListBox1.itemindex]);
  calcCubes();

  form4.Button2.visible := TRUE;
  form4.Button3.visible := TRUE;
  form4.ff := self;

  Label3.Caption := ListBox1.Items[ListBox1.itemindex];
  majlistbarre(-1);
  Button2.enabled := TRUE;
  Button3.enabled := TRUE;
end;

procedure TForm2.ListView1Change(Sender: TObject; Item: TListItem; Change: TItemChange);
begin
  deactiveCubes;
  activeCube(ListView1.itemindex);
  ListView1SelectItem(Sender, ListView1.Selected, TRUE);
end;

procedure TForm2.ListView1Click(Sender: TObject);
begin
  ListView1SelectItem(Sender, ListView1.Selected, TRUE);
end;

procedure TForm2.ListView1SelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
  if ListView1.Selected = nil then
  begin
    Button4.enabled := False;
    morceauactif := nil;
  end
  else
  begin
    Button4.enabled := TRUE;
    morceauactif := Tmorceau(barreactive.morceaux.Objects[ListView1.itemindex]);
  end;
  PaintBox2.Repaint;
  PaintBox1.Repaint;
end;

procedure TForm2.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; x, y: integer);
var
  I: integer;
begin
  for I := 0 to cubes.Count - 1 do
    if Tcube(cubes[I]).clic(x, y) then
    begin
      ListView1.itemindex := Tcube(cubes[I]).id;
    end;
end;

procedure TForm2.PaintBox1Paint(Sender: TObject);
var
  I: integer;
begin
  if ListBox1.itemindex < 0 then
    barreactive := nil;
  if barreactive = nil then
    exit;
  for I := 0 to cubes.Count - 1 do
  begin
    Tcube(cubes[I]).draw(PaintBox1.canvas);
  end;
  (*
    coeff := (PaintBox1.Width - 6) / barreactive.longueur;
    with PaintBox1.canvas do
    begin
    brush.Color := clblack;
    roundrect(0, 0, PaintBox1.Width, PaintBox1.Height, 5, 5);
    brush.Color := clappworkspace;
    roundrect(1, 1, PaintBox1.Width - 1, PaintBox1.Height - 1, 5, 5);
    end;
    tailletot := 0;
    posx := 3;
    for I := 0 to barreactive.morceaux.Count - 1 do
    begin
    with PaintBox1.canvas do
    begin
    if I = ListView1.itemindex then
    begin
    brush.Color := clLime;
    pen.Color := clGreen;
    end
    else
    begin
    brush.Color := clAqua;
    pen.Color := clBlue;
    end;
    roundrect(trunc(posx*coeff),3,trunc((posx+Tmorceau(barreactive.morceaux.Objects[i]).longueur)*coeff),paintbox1.Height-3,2,2);
    posx := posx + Tmorceau(barreactive.morceaux.Objects[I]).longueur;
    brush.Color := clRed;
    roundrect(trunc(posx * coeff), 3, trunc((posx + SCIE) * coeff), PaintBox1.Height - 3, 2, 2);
    posx := posx + SCIE;
    end;
    tailletot := tailletot + Tmorceau(barreactive.morceaux.Objects[I]).longueur + SCIE;
    end;
    *)
end;

procedure TForm2.PaintBox2Paint(Sender: TObject);
var
  xO, yO, larg, zoom: single;
  I, xCote, xDecal, xT: integer;
begin
  zoom := 1;
  if morceauactif = nil then
    exit;
  // selon profil�
  larg := 40;
  // ini
  xO := 25;
  yO := 25;
  zoom := (PaintBox2.Height - (yO * 2)) / morceauactif.longueur;
  with PaintBox2.canvas do
  begin
    // fond btnface
    brush.Color := clbtnface;
    brush.Style := bssolid;
    pen.Style := psclear;
    rectangle(cliprect);
    // rectangle profil�
    pen.Style := pssolid;
    pen.Color := clBlack;
    pen.Width := 2;
    brush.Color := clsilver;
    rectangle(round(xO), round(yO), round(xO + (zoom * larg)), round(yO + (zoom * morceauactif.longueur)));
    // percages
    font.Color := clBlue;
    pen.Width := 1;
    brush.Style := bsclear;
    font.Size := 8;
    xDecal := round(larg);
    xCote := round(xO + (zoom * (larg / 2)));
    for I := 0 to morceauactif.percages.Count - 1 do
    begin
      moveTo(round(xO + (zoom * (larg / 2) - 10)), round(yO + (zoom * strtofloat(morceauactif.percages.ValueFromIndex[I]))));
      lineTo(round(xO + (zoom * (larg / 2) + 10)), round(yO + (zoom * strtofloat(morceauactif.percages.ValueFromIndex[I]))));
      moveTo(round(xO + (zoom * (larg / 2))), round(yO + (zoom * strtofloat(morceauactif.percages.ValueFromIndex[I])) - 10));
      lineTo(round(xO + (zoom * (larg / 2))), round(yO + (zoom * strtofloat(morceauactif.percages.ValueFromIndex[I])) + 10));
      textOut(round(xO + (zoom * (larg / 2))) + 5, round(yO + (zoom * strtofloat(morceauactif.percages.ValueFromIndex[I]))) + 5, '#' + morceauactif.percages.Names[I]);
      xT := coteV(PaintBox2.canvas, yO, round(yO + (zoom * strtofloat(morceauactif.percages.ValueFromIndex[I]))), xCote, xDecal, morceauactif.percages.ValueFromIndex[I]);
      xDecal := xDecal + (xT * 2);
    end;
    // xT:=
    coteV(PaintBox2.canvas, yO, round(yO + (zoom * morceauactif.longueur)), xCote, xDecal, floattostr(morceauactif.longueur));
  end;
  (*
    if measuring=1 then with PaintBox1.canvas do
    begin
    textout(0,0,'Indiquez un 2�me point pour mesurer...');
    end;
    if measuring=2 then with PaintBox1.canvas do
    begin
    textout(0,0,floattostr((abs(endpoint.Y-startPoint.Y))/zoom)+'mm environ');
    measuring:=0;
    end;
    *)
end;

function TForm2.coteV(c: Tcanvas; yStart, yEnd: single; xAxe, xPos: integer; text: string): integer;
var
  f: integer;
begin
  f := 5;
  with c do
  begin
    pen.Style := psDashDot;
    // axe haut
    moveTo(xAxe, round(yStart));
    lineTo(xAxe + xPos, round(yStart));
    // axe bas
    moveTo(xAxe, round(yEnd));
    lineTo(xAxe + xPos, round(yEnd));
    // ligne de cote
    pen.Style := pssolid;
    moveTo(xAxe + xPos, round(yStart));
    lineTo(xAxe + xPos, round(yEnd));
    // fleche haut
    moveTo(xAxe + xPos, round(yStart));
    lineTo(xAxe + xPos - f, round(yStart) + f);
    moveTo(xAxe + xPos, round(yStart));
    lineTo(xAxe + xPos + f, round(yStart) + f);
    // fleche bas
    moveTo(xAxe + xPos, round(yEnd));
    lineTo(xAxe + xPos - f, round(yEnd) - f);
    moveTo(xAxe + xPos, round(yEnd));
    lineTo(xAxe + xPos + f, round(yEnd) - f);
    // texte de cote
    textOut(xAxe + xPos + 5, round(yStart + ((yEnd - yStart) / 2)), text);
  end;
  Result := c.TextWidth(text);
end;

procedure TForm2.calcCubes();
var
  I: integer;
  tailletot, coeff, posx: single;
begin
  if barreactive = nil then
    exit;
  clearCubes;
  coeff := (PaintBox1.Width - 6) / barreactive.longueur;
  tailletot := 0;
  posx := 3;
  for I := 0 to barreactive.morceaux.Count - 1 do
  begin
    cubes.Add(Tcube.create(rect(trunc(posx * coeff), 3, trunc((posx + Tmorceau(barreactive.morceaux.Objects[I]).longueur) * coeff), PaintBox1.Height - 3), I));
    posx := posx + Tmorceau(barreactive.morceaux.Objects[I]).longueur;
    cubes.Add(Tcube.create(rect(trunc(posx * coeff), 3, trunc((posx + SCIE) * coeff), PaintBox1.Height - 3), I));
    posx := posx + SCIE;
    tailletot := tailletot + Tmorceau(barreactive.morceaux.Objects[I]).longueur + SCIE;
  end;
end;

procedure TForm2.PopupMenu1Popup(Sender: TObject);
begin
  if ListBox1.itemindex < 0 then
  begin
    Editer1.enabled := False;
    Supprimer1.enabled := False;
  end
  else
  begin
    Editer1.enabled := TRUE;
    Supprimer1.enabled := TRUE;
  end;
end;

procedure TForm2.PopupMenu2Popup(Sender: TObject);
begin
  if ListView1.Selected = nil then
  begin
    Editer2.enabled := False;
    Supprimer2.enabled := False;
  end
  else
  begin
    Editer2.enabled := TRUE;
    Supprimer2.enabled := TRUE;
  end;
end;

procedure TForm2.setCaption;
begin
  if FbeSaved then
    Caption := '*' + Ftitre
  else
    Caption := Ftitre;
end;

procedure TForm2.setfilename(value: string);
begin
  Ffilename := value;
  titre := extractfilename(value);
end;

procedure TForm2.setTitre(value: string);
begin
  Ftitre := value;
  setCaption;
end;

{ Tcube }

procedure Tcube.active;
begin
  Factive := TRUE;
end;

function Tcube.clic(x, y: integer): boolean;
begin
  Result := (x > Frect.Left) and (x < Frect.right);
end;

constructor Tcube.create(r: Trect; id: integer);
begin
  Frect := r;
  Fid := id;
end;

procedure Tcube.deactive;
begin
  Factive := False;
end;

procedure Tcube.draw(canvas: Tcanvas);
begin
  with canvas do
  begin
    if Factive then
    begin
      brush.Color := clLime;
      pen.Color := clGreen;
    end
    else
    begin
      brush.Color := clAqua;
      pen.Color := clBlue;
    end;
    roundrect(Frect.Left, Frect.Top, Frect.right, Frect.Bottom, 2, 2);
  end;
end;

end.
