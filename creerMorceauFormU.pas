unit creerMorceauFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Menus, barresU, functions, ajoutPercageFormU;

type
  TForm5 = class(TForm)
    ListView1: TListView;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;
    PopupMenu1: TPopupMenu;
    Supprimer1: TMenuItem;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Button3: TButton;
    Label8: TLabel;
    Edit4: TEdit;
    Label9: TLabel;
    Memo1: TMemo;
    Label10: TLabel;
    ComboBox1: TComboBox;
    Label11: TLabel;
    ComboBox2: TComboBox;
    Button4: TButton;
    Button6: TButton;
    Button5: TButton;
    Memo2: TMemo;
    Button7: TButton;
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Supprimer1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Edit4Exit(Sender: TObject);
    procedure ComboBox1Exit(Sender: TObject);
    procedure ListView1Change(Sender: TObject; Item: TListItem; Change: TItemChange);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
    Fm: Tmorceau;
    FcanFreeMorceau: boolean;
    FformPercage: Tform3;
    FnomsPercages: Tstringlist;
    procedure majlistPercages;
  public
    { Public declarations }
    editing: boolean;
    callback: TnotifyEvent;
    procedure nouveau;
    procedure edit(const morceau: Tmorceau);
  end;

var
  Form5: TForm5;

implementation

uses listeMorceauxFormU;
{$R *.dfm}

procedure TForm5.edit(const morceau: Tmorceau);
var
  I: Integer;
  it: TListItem;
begin
  if morceau = nil then
  begin
    showmessage('Pas de morceau � �diter!');
    free;
    exit;
  end;
  caption := 'Editer le morceau';
  // ne pas supprimer le morceau en fermant car il est dans la liste des morceaux (form4)
  FcanFreeMorceau := False;
  // overwrite
  editing := True;
  // morceau en cours = morceau en param�tre
  Fm := morceau;
  // maj des inputs
  Edit4.text := Fm.nom;
  // Memo1.lines := Fm.description;
  Edit1.text := floattostr(Fm.longueur);
  // Edit2.text := inttostr(Fm.anglea);
  // Edit3.text := inttostr(Fm.angleb);

  ComboBox1.ItemIndex := Fm.codeProfil;

  ComboBox2.text := Fm.gamme;

  majlistPercages;
  Button1.enabled := True;
  // affichage
  showModal;
end;

procedure TForm5.nouveau;
begin
  // peut supprimer le morceau en formant, doit mette � jour la liste des morceaux (form4)
  FcanFreeMorceau := True;
  // overwrite
  editing := False;
  // nouveau morceau en cours
  Fm := Tmorceau.Create;
  caption := 'Cr�er un morceau';
  // maj des inputs
  Edit4.text := '';
  Memo1.clear;
  Edit1.text := '';
  Edit2.text := '0';
  Edit3.text := '0';
  ListView1.items.clear;
  Button1.enabled := False;
  // affichage
  showModal;
end;

procedure TForm5.majlistPercages;
var
  I: Integer;
  it: TListItem;
begin
  ListView1.items.BeginUpdate;
  ListView1.items.clear;
  if Fm.percages.Count > 0 then
  begin
    for I := 0 to Fm.percages.Count - 1 do
    begin
      it := ListView1.items.Add;
      it.caption := Fm.percages.Names[I];
      it.subitems.Add(Fm.percages.ValueFromIndex[I]);
      it.subitems.Add(FnomsPercages[strtoint(Fm.percages.Names[I])])
    end;
  end;
  ListView1.items.EndUpdate;
end;

procedure TForm5.Button1Click(Sender: TObject);
begin
  FformPercage := Tform3.Create(application);
  FformPercage.nouveau(Fm);
  // form en showmodal, attend la fermeture pour executer la suite
  majlistPercages;
end;

procedure TForm5.Button2Click(Sender: TObject);
var
  I: Integer;
  f: file;
begin
  if Edit4.text = '' then
    exit;
  if Edit1.text = '' then
    exit;
  if not IsValidFileName(Edit4.text) then
  begin
    showmessage('Nom invalide' + #13 + 'Ne dois pas contenir de caract�res sp�ciaux' + #13 + '\ / : * ? " < > |');
    exit;
  end;
  Fm.nom := Edit4.text;
  Fm.gamme := ComboBox2.text;
  Fm.codeProfil := ComboBox1.ItemIndex;
  // Fm.description.assign(Memo1.lines);
  Fm.longueur := strtofloat(Edit1.text);
  (* if Edit2.text = '' then
    Edit2.text := '0';
    if Edit3.text = '' then
    Edit3.text := '0';
    Fm.anglea := strtoint(Edit2.text);
    Fm.angleb := strtoint(Edit3.text);
    *)
  if not Fm.saveToFile(extractfilepath(application.ExeName) + 'morceaux/' + Fm.nom + '.txt', editing) then
  begin
    showmessage('Probl�me lors de l''enregistrement de ' + extractfilepath(application.ExeName) + 'morceaux/' + Fm.nom + '.txt');
  end;
  close;
end;

procedure TForm5.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TForm5.Button4Click(Sender: TObject);
begin
  Button4.enabled := False;
  Supprimer1Click(Sender);
end;

procedure TForm5.Button5Click(Sender: TObject);
var
  I: Integer;
begin
  if not Memo2.Visible then
    Memo2.Visible := True;
  I := ListView1.ItemIndex + 1;
  if I > ListView1.items.Count - 1 then
    exit;
  // vers le bas
  Fm.percages.Exchange(ListView1.ItemIndex, I);
  majlistPercages;
  ListView1.ItemIndex := I;
end;

procedure TForm5.Button6Click(Sender: TObject);
var
  I: Integer;
begin
  if not Memo2.Visible then
    Memo2.Visible := True;
  I := ListView1.ItemIndex - 1;
  if I < 0 then
    exit;
  // vers le haut
  Fm.percages.Exchange(ListView1.ItemIndex, I);
  majlistPercages;
  ListView1.ItemIndex := I;
end;

procedure TForm5.Button7Click(Sender: TObject);
begin
  if Memo2.Visible then
    Memo2.Visible := False;
  Fm.percages.CustomSort(sortPercagesPosInc);
  majlistPercages;
end;

procedure TForm5.ComboBox1Change(Sender: TObject);
begin
  Fm.codeProfil := TComboBox(Sender).ItemIndex;
end;

procedure TForm5.ComboBox1Exit(Sender: TObject);
begin
  Button1.enabled := (Edit1.text <> '') and (ComboBox1.ItemIndex <> 0);
  Button2.enabled := (Edit1.text <> '') and (Edit4.text <> '') and (ComboBox1.ItemIndex <> 0);
end;

procedure TForm5.Edit1Change(Sender: TObject);
var
  I: Integer;
begin
  if not tryStrToInt(Edit1.text, I) then
    exit;
  Fm.longueur := strtoint(Edit1.text);
end;

procedure TForm5.Edit1Exit(Sender: TObject);
begin
  Button1.enabled := (Edit1.text <> '') and (ComboBox1.ItemIndex <> 0);
  Button2.enabled := (Edit1.text <> '') and (Edit4.text <> '') and (ComboBox1.ItemIndex <> 0);
end;

procedure TForm5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := cafree;
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
  Memo2.Font.Color := clred;
  FnomsPercages := Tstringlist.Create;
  FnomsPercages.LoadFromFile(extractfilepath(application.ExeName) + 'codepercages.txt');
  ComboBox1.items.LoadFromFile(extractfilepath(application.ExeName) + 'codeprofiles.txt');
end;

procedure TForm5.FormDestroy(Sender: TObject);
begin
  if assigned(Fm) and FcanFreeMorceau then
    Fm.free;
  if assigned(callback) and FcanFreeMorceau then
    callback(Sender);
  FnomsPercages.free;
end;

procedure TForm5.FormShow(Sender: TObject);
begin
  Edit4.SetFocus;
end;

procedure TForm5.ListView1Change(Sender: TObject; Item: TListItem; Change: TItemChange);
begin
  Button4.enabled := ListView1.Selected <> nil;
end;

procedure TForm5.PopupMenu1Popup(Sender: TObject);
begin
  Supprimer1.enabled := ListView1.Selected <> nil;
end;

procedure TForm5.Supprimer1Click(Sender: TObject);
begin
  if ListView1.Selected = nil then
    exit;
  Fm.percages.Delete(ListView1.ItemIndex);
  majlistPercages;
end;

procedure TForm5.Edit4Exit(Sender: TObject);
begin
  Button1.enabled := (Edit1.text <> '') and (ComboBox1.ItemIndex <> 0);
  Button2.enabled := (Edit1.text <> '') and (Edit4.text <> '') and (ComboBox1.ItemIndex <> 0);
end;

end.
