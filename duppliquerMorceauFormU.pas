unit duppliquerMorceauFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm11 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Fnom: string;
  public
    { Public declarations }
    procedure dupp(nom: string);
  end;

var
  Form11: TForm11;

implementation

{$R *.dfm}

procedure TForm11.dupp(nom: string);
begin
  Fnom := nom;
  Edit1.text := nom;
  Button1.enabled := (trim(Edit1.text) <> '') and (trim(Edit1.text) <> Fnom);
end;

procedure TForm11.Edit1Change(Sender: TObject);
begin
  Button1.enabled := (trim(Edit1.text) <> '') and (trim(Edit1.text) <> Fnom);
end;

procedure TForm11.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
    key:=#0;
    button1click(sender);
  end;
end;

procedure TForm11.Button1Click(Sender: TObject);
begin
  modalResult := mrOk;
end;

procedure TForm11.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action:=cafree;
end;

procedure TForm11.FormCreate(Sender: TObject);
begin
  Edit1.Clear;
end;

end.
