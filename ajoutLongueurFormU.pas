unit ajoutLongueurFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, barresU, projetFormU;

type
  TForm10 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    ff:Tform2;
  end;

var
  Form10: TForm10;

implementation

{$R *.dfm}

procedure TForm10.Button1Click(Sender: TObject);
var
  m:Tmorceau;
  id:integer;
begin
  if edit1.Text='' then exit;
  if strtoint(edit1.Text)=0 then exit;

  if ff.barreactive=nil then exit;
  try
    m:=Tmorceau.create;
    m.nom:=edit1.Text;
    m.longueur:=strtoint(edit1.text);
  except
    raise Exception.Create('Impossible de cr�er le morceau!');
    exit;
  end;
  if m.longueur>ff.longueurDispo-SCIE then
  begin
    showmessage('Le morceau (plus sa coupe) est trop long pour le reste de la barre.');
    exit;
  end;
  try
    id:=ff.barreactive.morceaux.AddObject(m.nom,m);
  except
    raise Exception.Create('Tform4.Button2Click'+#13+'Impossible d''ajouter le morceau � la barre!');
  end;
  ff.majlistbarre(id);
  ff.beSaved:=TRUE;
end;

procedure TForm10.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm10.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
    key:=#0;
    button1click(sender);
  end;
end;

procedure TForm10.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action:=cafree;
end;

end.
