unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, functions;

type
  TForm9 = class(TForm)
    Label1: TLabel;
    Memo1: TMemo;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;
  lh:integer;

implementation

{$R *.dfm}

procedure TForm9.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=cafree;
end;

procedure TForm9.FormCreate(Sender: TObject);
begin
  label2click(sender);
end;

procedure TForm9.FormResize(Sender: TObject);
begin
    memo1.Height:=height-52-40;
    memo1.Width:=width-15;
end;

procedure TForm9.FormShow(Sender: TObject);
begin
  label1.Caption:='Editeur de barre pour machine CUMAX. Version '+getversion;
  memo1.Lines.LoadFromFile(extractfilepath(application.ExeName)+'changelog.txt');
end;

procedure TForm9.Label2Click(Sender: TObject);
begin
  if memo1.visible then
  begin
    memo1.Visible:=FALSE;
    label2.Caption:='Afficher les modifications';
    lh:=height;
    height:=110;
  end else begin
    memo1.Visible:=TRUE;
    label2.Caption:='Masquer les modifications';
    height:=lh;
    memo1.Top:=52;
    memo1.Height:=height-52-40;
    memo1.Width:=width-15;
  end;
end;

end.
