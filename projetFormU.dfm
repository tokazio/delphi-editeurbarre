object Form2: TForm2
  Left = 0
  Top = 0
  ClientHeight = 591
  ClientWidth = 1123
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 271
    Top = 0
    Height = 591
    ExplicitLeft = 500
    ExplicitTop = 224
    ExplicitHeight = 100
  end
  object Splitter2: TSplitter
    Left = 737
    Top = 0
    Height = 591
    Align = alRight
    ExplicitLeft = 275
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 271
    Height = 591
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 0
    object ListBox1: TListBox
      Left = 0
      Top = 41
      Width = 271
      Height = 550
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 23
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      OnClick = ListBox1Click
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 271
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        271
        41)
      object Label2: TLabel
        Left = 10
        Top = 16
        Width = 76
        Height = 13
        Caption = 'Liste des barres'
      end
      object Button1: TButton
        Left = 180
        Top = 8
        Width = 31
        Height = 25
        Action = Form8.nouvellebarre
        Anchors = [akTop, akRight]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Button3: TButton
        Left = 220
        Top = 8
        Width = 31
        Height = 25
        Hint = 'Retirer la barre s'#233'l'#233'ctionn'#233'e'
        Caption = '-'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = Button3Click
      end
    end
  end
  object Panel3: TPanel
    Left = 274
    Top = 0
    Width = 463
    Height = 591
    Align = alClient
    BevelOuter = bvNone
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 1
    DesignSize = (
      463
      591)
    object ListView1: TListView
      Left = 0
      Top = 41
      Width = 463
      Height = 419
      Align = alClient
      Columns = <
        item
          AutoSize = True
          Caption = 'Morceau'
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ReadOnly = True
      ParentFont = False
      PopupMenu = PopupMenu2
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = ListView1Change
      OnClick = ListView1Click
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 463
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      DoubleBuffered = False
      ParentDoubleBuffered = False
      TabOrder = 1
      DesignSize = (
        463
        41)
      object Label1: TLabel
        Left = 10
        Top = 16
        Width = 46
        Height = 13
        Caption = 'D'#233'tail de:'
      end
      object Label3: TLabel
        Left = 60
        Top = 16
        Width = 175
        Height = 13
        Caption = 'Choisir une barre dans la liste...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Button4: TButton
        Left = 425
        Top = 8
        Width = 31
        Height = 25
        Hint = 'Retirer le morceau s'#233'l'#233'ctionn'#233
        Anchors = [akTop, akRight]
        Caption = '-'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = Button4Click
      end
      object Button7: TButton
        Left = 272
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '+ longueur'
        TabOrder = 1
        OnClick = Button7Click
      end
      object Button2: TButton
        Left = 352
        Top = 8
        Width = 49
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '+ liste'
        TabOrder = 2
        OnClick = Button2Click
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 540
      Width = 463
      Height = 51
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        463
        51)
      object Label4: TLabel
        Left = 10
        Top = 8
        Width = 153
        Height = 13
        Caption = 'Choisir une barre dans la liste...'
      end
      object Label5: TLabel
        Left = 10
        Top = 28
        Width = 153
        Height = 13
        Caption = 'Choisir une barre dans la liste...'
      end
      object Button5: TButton
        Left = 385
        Top = 16
        Width = 71
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Descendre'
        TabOrder = 0
        OnClick = Button5Click
      end
      object Button6: TButton
        Left = 315
        Top = 16
        Width = 65
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Monter'
        TabOrder = 1
        OnClick = Button6Click
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 460
      Width = 463
      Height = 80
      Align = alBottom
      TabOrder = 3
      DesignSize = (
        463
        80)
      object PaintBox1: TPaintBox
        Left = 10
        Top = 24
        Width = 436
        Height = 33
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnMouseUp = PaintBox1MouseUp
        OnPaint = PaintBox1Paint
        ExplicitWidth = 691
      end
    end
    object Panel7: TPanel
      Left = 273
      Top = 412
      Width = 185
      Height = 41
      Anchors = [akRight, akBottom]
      Caption = 'La chute est <30mm'
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 4
      Visible = False
    end
  end
  object Panel8: TPanel
    Left = 740
    Top = 0
    Width = 383
    Height = 591
    Align = alRight
    BevelOuter = bvNone
    Caption = 'Panel8'
    ParentBackground = False
    ShowCaption = False
    TabOrder = 2
    object PaintBox2: TPaintBox
      Left = 0
      Top = 0
      Width = 383
      Height = 591
      Align = alClient
      OnPaint = PaintBox2Paint
      ExplicitLeft = 40
      ExplicitTop = 244
      ExplicitWidth = 105
      ExplicitHeight = 105
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 160
    Top = 388
    object Editer1: TMenuItem
      Caption = 'Editer'
      OnClick = Editer1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Supprimer1: TMenuItem
      Caption = 'Supprimer'
      OnClick = Supprimer1Click
    end
  end
  object PopupMenu2: TPopupMenu
    OnPopup = PopupMenu2Popup
    Left = 580
    Top = 248
    object Dupliquer1: TMenuItem
      Caption = 'Dupliquer ce morceau'
      OnClick = Dupliquer1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Editer2: TMenuItem
      Caption = 'Editer ce morceau'
      OnClick = Editer2Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Supprimer2: TMenuItem
      Caption = 'Supprimer'
      OnClick = Supprimer2Click
    end
  end
end
