object Form5: TForm5
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Cr'#233'er un morceau'
  ClientHeight = 500
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    470
    500)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 77
    Width = 126
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Longueur du morceau:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 10
    Top = 101
    Width = 103
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Angle de coupe haut:'
    Visible = False
  end
  object Label3: TLabel
    Left = 270
    Top = 101
    Width = 98
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Angle de coupe bas:'
    Visible = False
  end
  object Label4: TLabel
    Left = 10
    Top = 125
    Width = 44
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Per'#231'ages'
  end
  object Label5: TLabel
    Left = 280
    Top = 77
    Width = 16
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'mm'
  end
  object Label6: TLabel
    Left = 210
    Top = 101
    Width = 5
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = #176
    Visible = False
  end
  object Label7: TLabel
    Left = 440
    Top = 101
    Width = 5
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = #176
    Visible = False
  end
  object Label8: TLabel
    Left = 10
    Top = 8
    Width = 28
    Height = 13
    Caption = 'Nom:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 10
    Top = 56
    Width = 57
    Height = 13
    Caption = 'Description:'
    Visible = False
  end
  object Label10: TLabel
    Left = 10
    Top = 36
    Width = 39
    Height = 13
    Caption = 'Profil'#233':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 233
    Top = 36
    Width = 47
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Gamme:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitLeft = 278
  end
  object ListView1: TListView
    Left = 10
    Top = 144
    Width = 451
    Height = 250
    Anchors = [akLeft, akRight, akBottom]
    Columns = <
      item
        AutoSize = True
        Caption = 'Code'
      end
      item
        AutoSize = True
        Caption = 'Position'
      end
      item
        Caption = 'Description du per'#231'age'
        Width = 300
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    RowSelect = True
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 0
    ViewStyle = vsReport
    OnChange = ListView1Change
  end
  object Button1: TButton
    Left = 10
    Top = 397
    Width = 41
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '+'
    Enabled = False
    TabOrder = 1
    OnClick = Button1Click
    ExplicitTop = 388
  end
  object Button2: TButton
    Left = 373
    Top = 468
    Width = 85
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Enregistrer'
    Enabled = False
    TabOrder = 2
    OnClick = Button2Click
    ExplicitTop = 459
  end
  object Edit1: TEdit
    Left = 150
    Top = 73
    Width = 121
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 3
    OnChange = Edit1Change
    OnExit = Edit1Exit
  end
  object Edit2: TEdit
    Left = 150
    Top = 97
    Width = 51
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 4
    Text = '0'
    Visible = False
  end
  object Edit3: TEdit
    Left = 380
    Top = 97
    Width = 51
    Height = 21
    Anchors = [akLeft, akBottom]
    TabOrder = 5
    Text = '0'
    Visible = False
  end
  object Button3: TButton
    Left = 279
    Top = 468
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    TabOrder = 6
    OnClick = Button3Click
    ExplicitTop = 459
  end
  object Edit4: TEdit
    Left = 55
    Top = 4
    Width = 410
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 7
    OnExit = Edit4Exit
  end
  object Memo1: TMemo
    Left = 80
    Top = 56
    Width = 380
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    ScrollBars = ssVertical
    TabOrder = 8
    Visible = False
  end
  object ComboBox1: TComboBox
    Left = 55
    Top = 32
    Width = 170
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 9
    Text = 'Choisir...'
    OnChange = ComboBox1Change
    OnExit = ComboBox1Exit
    Items.Strings = (
      'Choisir...'
      '01- Monoprofil ('#224' plat vers le bas)'
      '02- Biprofil'
      '03- Cache rainure'
      '04- Tube MC diam'#232'tre 50'
      '05- Poteau diam'#232'tre 40 (rainure en bas)'
      '06- Tube Traverse diam'#232'tre 16')
  end
  object ComboBox2: TComboBox
    Left = 287
    Top = 32
    Width = 175
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 10
    Text = 'Choisir...'
    Items.Strings = (
      'Chrome'
      'Baludesign'
      'Monoprofil')
  end
  object Button4: TButton
    Left = 58
    Top = 397
    Width = 41
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '-'
    Enabled = False
    TabOrder = 11
    OnClick = Button4Click
    ExplicitTop = 388
  end
  object Button6: TButton
    Left = 319
    Top = 397
    Width = 65
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Monter'
    TabOrder = 12
    OnClick = Button6Click
    ExplicitTop = 388
  end
  object Button5: TButton
    Left = 389
    Top = 397
    Width = 71
    Height = 25
    Anchors = [akRight]
    Caption = 'Descendre'
    TabOrder = 13
    OnClick = Button5Click
    ExplicitTop = 388
  end
  object Memo2: TMemo
    Left = 12
    Top = 429
    Width = 445
    Height = 33
    Anchors = [akLeft, akRight, akBottom]
    BevelInner = bvNone
    BorderStyle = bsNone
    Color = clBtnFace
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      
        'Attention, les positions des per'#231'ages doivent '#234'tre dans l'#39'ordre ' +
        'croissant afin '
      'd'#39#233'viter que la pince de la machine ne recule!')
    ParentFont = False
    TabOrder = 14
    Visible = False
    ExplicitTop = 420
  end
  object Button7: TButton
    Left = 236
    Top = 397
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Ordonner'
    TabOrder = 15
    OnClick = Button7Click
    ExplicitTop = 388
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 220
    Top = 432
    object Supprimer1: TMenuItem
      Caption = 'Supprimer'
      OnClick = Supprimer1Click
    end
  end
end
