program barreeditor;

uses
  windows,
  Forms,
  messages,
  dialogs,
  sysutils,
  projetFormU in 'projetFormU.pas' { Form2 } ,
  listeMorceauxFormU in 'listeMorceauxFormU.pas' { Form4 } ,
  creerMorceauFormU in 'creerMorceauFormU.pas' { Form5 } ,
  creerBarreFormU in 'creerBarreFormU.pas' { Form1 } ,
  barresU in 'barresU.pas',
  ajoutPercageformU in 'ajoutPercageformU.pas' { Form3 } ,
  functions in 'functions.pas',
  Unit6 in 'Unit6.pas' { Form6 } ,
  exportFormU in 'exportFormU.pas' { Form7 } ,
  mainFormU in 'mainFormU.pas' { Form8 } ,
  Unit9 in 'Unit9.pas' { Form9 } ,
  ajoutLongueurFormU in 'ajoutLongueurFormU.pas' { Form10 } ,
  duppliquerMorceauFormU in 'duppliquerMorceauFormU.pas' { Form11 } ,
  splashFormU in 'splashFormU.pas' { Form12 } ;

const
  WM_PARAMETRE = WM_USER + 1;

var
  Texte: Atom;
  h: Thandle;
  f12: Tform12;
{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;

  CreateMutex(nil, False, PChar(ExtractFileName(Application.ExeName)));
  if (GetLastError = ERROR_ALREADY_EXISTS) then
  begin
    if ParamStr(1) <> '' then
    begin
      Texte := GlobalAddAtom(PChar(ParamStr(1)));
      h := FindWindow(nil, PChar('CUMAX'));
      SendMessage(h, WM_PARAMETRE, 0, Texte);
    end;
  end
  else
  begin
    decimalSeparator := '.';
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    f12 := Tform12.Create(Application);
    f12.show;
    f12.update;
    Application.Title := 'Editeur CUMAX';
    Application.CreateForm(TForm8, Form8);
    Application.CreateForm(TForm4, Form4);
    f12.free;
    Application.Run;
  end;

end.
