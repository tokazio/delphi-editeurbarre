object Form8: TForm8
  Left = 0
  Top = 0
  Caption = 'CUMAX'
  ClientHeight = 602
  ClientWidth = 1057
  Color = clAppWorkSpace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 583
    Width = 1057
    Height = 19
    Panels = <
      item
        Width = 350
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object MainMenu1: TMainMenu
    Left = 770
    Top = 8
    object Fichier1: TMenuItem
      Caption = 'Editeur'
      object Nouveau1: TMenuItem
        Action = nouveau
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Enregistrer1: TMenuItem
        Action = enregistrer
      end
      object Enregistrersous1: TMenuItem
        Action = enregistrersous
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Rcents1: TMenuItem
        Caption = 'R'#233'cents'
        object TMenuItem
        end
      end
      object Ouvrir1: TMenuItem
        Action = ouvrir
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Exporter1: TMenuItem
        Action = exporter
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Importer1: TMenuItem
        Action = importer
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Quitter1: TMenuItem
        Action = quitter
      end
    end
    object Barres1: TMenuItem
      Caption = 'Barres'
      object Nouvellebarre1: TMenuItem
        Action = nouvellebarre
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object tempsfabrication1: TMenuItem
        Action = tempsfabrication
        Caption = 'Temps de fabrication'
      end
    end
    object Morceaux1: TMenuItem
      Caption = 'Morceaux'
      object Listedesmorceaux1: TMenuItem
        Action = listemorceaux
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object Nouveaumorceau1: TMenuItem
        Action = creermorceau
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object Importer2: TMenuItem
        Action = importermx
      end
      object Aemporter1: TMenuItem
        Action = emporter
      end
    end
    object Perages1: TMenuItem
      Caption = 'Per'#231'ages'
      object Editerlalistedescodes1: TMenuItem
        Action = listecodespercages
      end
    end
    object Profils1: TMenuItem
      Caption = 'Profil'#233's'
      object Editerlelistedescodes1: TMenuItem
        Action = listecodesprofiles
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object longueursprofiles1: TMenuItem
        Action = longueursprofiles
        Caption = 'Longueurs par d'#233'faut'
      end
    end
    object Affichage1: TMenuItem
      Caption = 'Affichage'
      OnClick = Affichage1Click
      object Cascade1: TMenuItem
        Tag = -1
        Action = mcascade
      end
      object Mosaqueverticale1: TMenuItem
        Tag = -1
        Action = mvt
      end
      object Mosaquehorizontale1: TMenuItem
        Tag = -1
        Action = mhz
      end
      object N11: TMenuItem
        Tag = -1
        Caption = '-'
      end
    end
    object N5: TMenuItem
      Caption = '?'
      object Aproposde1: TMenuItem
        Action = apropos
      end
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
      end>
    Images = ImageList1
    Left = 640
    Top = 16
    StyleName = 'Platform Default'
    object quitter: TAction
      Category = 'Editeur'
      Caption = 'Quitter'
      ShortCut = 16465
      OnExecute = quitterExecute
    end
    object enregistrer: TAction
      Category = 'Editeur'
      Caption = 'Enregistrer'
      Enabled = False
      ShortCut = 16467
      OnExecute = enregistrerExecute
    end
    object enregistrersous: TAction
      Category = 'Editeur'
      Caption = 'Enregistrer sous...'
      Enabled = False
      OnExecute = enregistrersousExecute
    end
    object ouvrir: TAction
      Category = 'Editeur'
      Caption = 'Ouvrir'
      ShortCut = 16463
      OnExecute = ouvrirExecute
    end
    object exporter: TAction
      Category = 'Editeur'
      Caption = 'Exporter'
      Enabled = False
      ShortCut = 16453
      OnExecute = exporterExecute
    end
    object nouvellebarre: TAction
      Category = 'Barres'
      Caption = 'Nouvelle barre'
      Enabled = False
      OnExecute = nouvellebarreExecute
    end
    object listemorceaux: TAction
      Category = 'Morceaux'
      Caption = 'Liste des morceaux'
      OnExecute = listemorceauxExecute
    end
    object creermorceau: TAction
      Category = 'Morceaux'
      Caption = 'Nouveau morceau'
      OnExecute = creermorceauExecute
    end
    object listecodespercages: TAction
      Category = 'Per'#231'ages'
      Caption = 'Liste des codes per'#231'ages'
      OnExecute = listecodespercagesExecute
    end
    object listecodesprofiles: TAction
      Category = 'Profiles'
      Caption = 'Liste des codes profil'#233's'
      OnExecute = listecodesprofilesExecute
    end
    object apropos: TAction
      Category = 'Aide'
      Caption = 'A propos de...'
      OnExecute = aproposExecute
    end
    object nouveau: TAction
      Category = 'Editeur'
      Caption = 'Nouveau'
      ShortCut = 16462
      OnExecute = nouveauExecute
    end
    object importer: TAction
      Category = 'Editeur'
      Caption = 'Importer'
      ShortCut = 16457
      Visible = False
      OnExecute = importerExecute
    end
    object tempsfabrication: TAction
      Category = 'Per'#231'ages'
      Caption = 'tempsfabrication'
      OnExecute = tempsfabricationExecute
    end
    object longueursprofiles: TAction
      Category = 'Profiles'
      Caption = 'longueursprofiles'
      OnExecute = longueursprofilesExecute
    end
    object emporter: TAction
      Category = 'Morceaux'
      Caption = 'A emporter...'
      Visible = False
      OnExecute = emporterExecute
    end
    object importermx: TAction
      Category = 'Morceaux'
      Caption = 'Importer...'
      Visible = False
      OnExecute = importermxExecute
    end
    object mvt: TAction
      Category = 'Affichage'
      Caption = 'Mosa'#239'que verticale'
      OnExecute = mvtExecute
    end
    object mhz: TAction
      Category = 'Affichage'
      Caption = 'Mosa'#239'que horizontale'
      OnExecute = mhzExecute
    end
    object mcascade: TAction
      Category = 'Affichage'
      Caption = 'Cascade'
      OnExecute = mcascadeExecute
    end
  end
  object ImageList1: TImageList
    DrawingStyle = dsTransparent
    Height = 32
    Width = 32
    Left = 372
    Top = 220
  end
  object OpenDialog: TOpenDialog
    Filter = 'Editeur de barre|*.cxe'
    Title = 'Ouvrir'
    Left = 310
    Top = 172
  end
  object ImportDialog: TOpenDialog
    Filter = 'CSV|*.CSV'
    Title = 'Importer un fichier de d'#233'coupe (.CSV)'
    Left = 50
    Top = 228
  end
  object SaveDialog: TSaveDialog
    Filter = 'Editeur de barre|*.cxe'
    Title = 'Enregistrer sous...'
    Left = 190
    Top = 120
  end
  object Timer1: TTimer
    Interval = 250
    OnTimer = Timer1Timer
    Left = 530
    Top = 208
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.mxf'
    FileName = 'mesmorceaux'
    Filter = 'mxf|*.mxf'
    Title = 'Morceaux '#224' emporter...'
    Left = 540
    Top = 112
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '.mxf'
    FileName = 'mesmorceaux'
    Filter = 'MXF|*.mxf'
    Title = 'Importer des morceaux...'
    Left = 440
    Top = 172
  end
end
