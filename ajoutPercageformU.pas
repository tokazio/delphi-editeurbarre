unit ajoutPercageformU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, barresU, ExtCtrls, functions;

type
  TForm3 = class(TForm)
    Label1: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    Button2: TButton;
    Label4: TLabel;
    Edit2: TEdit;
    Label5: TLabel;
    Edit3: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Edit4: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    CheckBox1: TCheckBox;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Bevel1: TBevel;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    Fm: Tmorceau;
    Fcodes: Tstringlist;
    function verifCode(codePercage: integer): integer;
  public
    { Public declarations }
    procedure nouveau(const morceau: Tmorceau);
  end;

var
  Form3: TForm3;

implementation

uses creerMorceauFormU;
{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
var
  j: integer;
  i: single;
begin
  if ComboBox1.ItemIndex < 1 then
    exit;
  if Fm.codeprofil <> verifCode(ComboBox1.ItemIndex) then
  begin
    showmessage('Ce num�ro de per�age n''est pas disponnible pour ce type de profil�!');
    exit;
  end;
  if CheckBox1.Checked then
  begin
    if Edit2.Text = '' then
      exit;
    if Edit3.Text = '' then
      exit;
    if Edit4.Text = '' then
      exit;
    if strtofloat(Edit2.Text) > Fm.longueur then
    begin
      showmessage('La position du 1er per�age d�passe la taille du morceau!');
      exit;
    end;
    j := 0;
    i := strtofloat(Edit2.Text);
    while i < Fm.longueur do
    begin
      Fm.percages.Add(inttostr(ComboBox1.ItemIndex) + '=' + floattostr(i));
      i := i + strtoint(Edit3.Text);
      if (strtoint(Edit4.Text) > 0) and (j = strtoint(Edit4.Text) - 1) then
        break;
      inc(j);
    end;
  end
  else
  begin
    if Edit1.Text = '' then
      exit;
    if strtofloat(Edit1.Text) > Fm.longueur then
    begin
      showmessage('La position du per�age d�passe la taille du morceau!');
      exit;
    end;
    Fm.percages.Add(inttostr(ComboBox1.ItemIndex) + '=' + Edit1.Text);
    Fm.percages.CustomSort(sortPercagesPosInc);
  end;
  close;
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm3.nouveau(const morceau: Tmorceau);
begin
  if morceau = nil then
  begin
    showmessage('Pas de morceau auquel ajouter un per�ages!');
    free;
    exit;
  end;
  Fm := morceau;
  showModal;
end;

procedure TForm3.CheckBox1Click(Sender: TObject);
begin
  Edit2.Enabled := CheckBox1.Checked;
  Edit3.Enabled := CheckBox1.Checked;
  Edit4.Enabled := CheckBox1.Checked;
  Edit1.Enabled := not CheckBox1.Checked;
end;

procedure TForm3.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    Button1Click(Sender);
  end;
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  Fcodes := Tstringlist.Create;
  Fcodes.LoadFromFile(extractfilepath(application.ExeName) + 'codepercagesbyprofiles.txt');
  ComboBox1.Items.LoadFromFile(extractfilepath(application.ExeName) + 'codepercages.txt');
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  Fcodes.free
end;

function TForm3.verifCode(codePercage: integer): integer;
begin
  Result := -1;
  try
    Result := strtoint(Fcodes.values[inttostr(codePercage)]);
  except
    showmessage('Le code per�age ne semble d�finis pour aucun profil�.' + #13 + 'Fichier: codepercagesbyprofiles.txt' + #13 + 'Code per�age: ' + inttostr(codePercage));
  end;
end;

procedure TForm3.FormShow(Sender: TObject);
begin
  ComboBox1.Text := 'Choisir...';
  Edit1.Text := '';
  ComboBox1.SetFocus;

  CheckBox1.Checked := FALSE;
  Edit2.Text := '0';
  Edit3.Text := '0';
  Edit4.Text := '0';
end;

end.
