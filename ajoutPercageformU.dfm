object Form3: TForm3
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'Ajouter un per'#231'age'
  ClientHeight = 245
  ClientWidth = 682
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    682
    245)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 12
    Width = 80
    Height = 13
    Caption = 'Code per'#231'age:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 10
    Top = 44
    Width = 48
    Height = 13
    Caption = 'Position:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 655
    Top = 44
    Width = 16
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'mm'
    ExplicitLeft = 260
  end
  object Label4: TLabel
    Left = 10
    Top = 64
    Width = 115
    Height = 13
    Caption = '(Depuis le haut/gauche)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGrayText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 10
    Top = 96
    Width = 659
    Height = 1
    Anchors = [akLeft, akTop, akRight]
    ExplicitWidth = 631
  end
  object Label11: TLabel
    Left = 280
    Top = 88
    Width = 123
    Height = 13
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'ou'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = False
    ExplicitWidth = 91
  end
  object GroupBox1: TGroupBox
    Left = 10
    Top = 116
    Width = 662
    Height = 85
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      '                                                                ' +
      ' '
    TabOrder = 5
    object Label10: TLabel
      Left = 294
      Top = 52
      Width = 6
      Height = 13
      Caption = 'x'
    end
    object Label5: TLabel
      Left = 10
      Top = 28
      Width = 62
      Height = 13
      Caption = '1er per'#231'age:'
    end
    object Label6: TLabel
      Left = 140
      Top = 28
      Width = 38
      Height = 13
      Caption = 'Entraxe'
    end
    object Label7: TLabel
      Left = 260
      Top = 28
      Width = 115
      Height = 13
      Caption = 'R'#233'p'#233'tition (1er compris)'
    end
    object Label8: TLabel
      Left = 74
      Top = 52
      Width = 16
      Height = 13
      Caption = 'mm'
    end
    object Label9: TLabel
      Left = 204
      Top = 52
      Width = 16
      Height = 13
      Caption = 'mm'
    end
    object Edit2: TEdit
      Left = 10
      Top = 48
      Width = 61
      Height = 21
      Enabled = False
      TabOrder = 0
      Text = '0'
    end
    object Edit3: TEdit
      Left = 140
      Top = 48
      Width = 61
      Height = 21
      Enabled = False
      TabOrder = 1
      Text = '0'
    end
    object Edit4: TEdit
      Left = 260
      Top = 48
      Width = 31
      Height = 21
      Enabled = False
      NumbersOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
  object ComboBox1: TComboBox
    Left = 100
    Top = 8
    Width = 546
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'Choisir...'
    Items.Strings = (
      'Choisir...'
      '01- Per'#231'ages diam 7 vertical axe-27 (Biprofil)'
      '02- Per'#231'ages diam 7 vertical axe+27 (Biprofil)'
      
        '03- Per'#231'ages double diam 7 vertical d'#233'bouchant rainure (Poteau d' +
        'iam 40)'
      '04- Per'#231'ages diam 5 vertical d'#233'bouchant (Monoprofil)'
      '05-'
      '06- Per'#231'ages diam 7+11 horizontal avant + arri'#232're (Biprofil)'
      '07- Per'#231'ages diam 5 vertical d'#233'bouchant (Poteau diam 40)'
      '08- Per'#231'ages 15mm diam 7 vertical (Biprofil)'
      '09- Per'#231'ages 15mm diam 7 vertical (Monoprofil)'
      '10- Per'#231'ages diam 7 horizontal 1'#232're paroie (Poteau diam 40)'
      '11- Per'#231'ages diam 7 horizontal d'#233'bouchant (Poteau diam 40)'
      ''
      ''
      ''
      ''
      '')
  end
  object Edit1: TEdit
    Left = 100
    Top = 40
    Width = 546
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnKeyPress = Edit1KeyPress
  end
  object Button1: TButton
    Left = 600
    Top = 212
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Enregistrer'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 510
    Top = 212
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    TabOrder = 3
    OnClick = Button2Click
  end
  object CheckBox1: TCheckBox
    Left = 30
    Top = 112
    Width = 181
    Height = 17
    Caption = 'Calculer automatiquement avec:'
    TabOrder = 4
    OnClick = CheckBox1Click
  end
end
