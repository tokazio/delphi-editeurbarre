unit exportFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, functions;

type
  TForm7 = class(TForm)
    Memo1: TMemo;
    Panel2: TPanel;
    Button1: TButton;
    FileSaveDialog1: TFileSaveDialog;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

uses mainFormU;

{$R *.dfm}

procedure TForm7.Button1Click(Sender: TObject);
begin
  filesavedialog1.DefaultFolder:=extractfilepath(form8.activeform.filename);
  filesavedialog1.FileName:=extractfilenameonly(extractfilename(form8.activeform.filename));
  if filesavedialog1.Execute then
  begin
    memo1.Lines.SaveToFile(uppercase(extractfilenameonly(filesavedialog1.FileName))+'.CSV');
  end;

end;

procedure TForm7.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action:=cafree;
end;

end.
