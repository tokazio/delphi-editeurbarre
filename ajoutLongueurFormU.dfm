object Form10: TForm10
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Ajouter une longueur'
  ClientHeight = 43
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  DesignSize = (
    266
    43)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 16
    Width = 49
    Height = 13
    Caption = 'Longueur:'
  end
  object Label2: TLabel
    Left = 156
    Top = 16
    Width = 16
    Height = 13
    Caption = 'mm'
  end
  object Edit1: TEdit
    Left = 70
    Top = 12
    Width = 81
    Height = 21
    TabOrder = 0
    Text = '0'
    OnKeyPress = Edit1KeyPress
  end
  object Button1: TButton
    Left = 186
    Top = 9
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Ajouter'
    TabOrder = 1
    OnClick = Button1Click
  end
end
